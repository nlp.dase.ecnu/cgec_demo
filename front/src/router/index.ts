import {
  createRouter,
  createWebHistory,
  Router,
  RouteRecordRaw,
} from "vue-router";
// import { createRouterGuards } from "./routerGuards";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/error_diagnose",
  },
  {
    path: "/error_diagnose",
    component: () => import("../views/a.vue"),
  },
  {
    path: "/com_eva",
    component: () => import("../views/b.vue"),
  },
  {
    path: "/data_analysis",
    component: () => import("../views/c.vue"),
  },
];

const router: Router = createRouter({
  history: createWebHistory(""),
  routes,
});

// export function setupRouter() {
//   // 创建路由守卫
//   createRouterGuards(router);
// }

export default router;
