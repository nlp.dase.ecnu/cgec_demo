# An Automatic Evaluation and Analysis Tool for Chinese Grammatical Error Correction

# 后端
[back](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back)文件夹中存放工具后端代码，以下说明基于[back](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back)文件夹

## Installation

运行以下代码以安装所需的库:

```
pip install -r requirements.txt
```
使用Python 3.7环境

## Datasets

包含如下测试集：

- [CTC2021](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/CTC2021)

- [FCGEC](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/FCGEC)

- [Hybrid](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/Hybrid)

- [NLPCC18](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/NLPCC_file)

- [NaCGEC](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/NaCGEC)

- [SIGHAN15](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/back/SIGHAN15)


## 细粒度评估模型预测结果

在```scripts/evaluate.py```中，修改以下参数：

```gold_file```：m2格式gold reference文件位置

```predict_segment_files```：经过分词的模型预测结果文件位置，每行一句纠正语句

```result_file```：输出结果文件位置，将各模型各错误类型的f0.5、precision、recall输出为csv文件

## 细粒度评估数据集

在```scripts/evaluate_dataset.py```中，修改以下参数：

```gold_file```：m2格式数据集文件位置

```out_file```：输出结果文件位置，将数据集各错误类型的占比输出为csv文件

## 生成m2格式细粒度错误分类自动标注

### 将平行原始与纠正语句自动标注为m2格式

在```scripts/generate_m2.py```中，修改以下参数：

```file_src```：源语句文件位置，每行一句源语句

```file_predict```：纠正语句文件位置，每行一句纠正语句

```file_out```：输出结果文件位置，输出m2格式自动标注文件

###  将m2格式自动标注为m2格式

在```scripts/generate_m2.py```中，注释原始main函数，使用最后三行，并修改以下参数：

```file_m2```：原始m2格式文件位置

```file_out```：输出结果文件位置，输出m2格式自动标注文件


## 网站后端
```scripts/server.py```：flask框架封装接口

服务器部署时使用以下代码：

```
python server.py
```

# 前端

[front](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/front)文件夹中存放工具后端代码，以下说明基于[front](https://gitlab.com/nlp.dase.ecnu/cgec_demo/-/tree/main/front)文件夹

## Installation
安装node环境以及yarn环境后，运行以下代码以安装所需的依赖包：

```
yarn
```

## 配置ip及端口
```vite.config.ts```中更改```target```以及```port```

## 运行
```
yarn dev
```