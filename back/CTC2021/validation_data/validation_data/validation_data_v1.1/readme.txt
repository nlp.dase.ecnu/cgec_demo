2022年4月19日更新

· 优化了1.0版本中未发现的错误

· 输入句子数：969条
· 含有错误的句子数：480条
	别字别字错误：280个
	冗余错误：59个
	缺失错误：88个
	乱序错误：11个
	语义重复：73个
	句式杂糅：27个
