def edit(sent_list, o_start, o_end, cor_toks, offset):
    sent_list[o_start + offset:o_end + offset] = cor_toks
    offset = offset - (o_end - o_start) + len(cor_toks)
    return sent_list, offset

if __name__ == "__main__":
    with open("qua_input.txt", "r", encoding="utf-8") as f_input, open("qua_labels.txt", "r", encoding="utf-8") as f_label, \
        open("CTC_valid.txt", "w", encoding="utf-8") as fo:
        src_data = {}
        tgt_data = {}
        for line in f_input.readlines():
            pid, src_text = line.strip().split("\t")
            src_data[pid] = src_text
        for line in f_label.readlines():
            labels = [i.strip() for i in line.strip().split(",")]
            pid = labels[0]
            if len(labels) == 2 and labels[1]=="-1": tgt_data[pid] = src_data[pid]
            else:
                src_list = [char for char in src_data[pid]]
                label_list = [labels[i:i+4] for i in range(1, len(labels)-1, 4)]
                offset = 0
                tgt_list = src_list
                for label in label_list:
                    o_start = int(label[0])
                    o_end = o_start + len(label[2])
                    cor_toks = [char for char in label[3]]
                    tgt_list, offset = edit(tgt_list, o_start, o_end, cor_toks, offset)
                tgt_data[pid] = "".join(tgt_list)
        for pid in src_data:
            fo.write(src_data[pid])
            fo.write("\t")
            fo.write(tgt_data[pid])
            fo.write("\n")
