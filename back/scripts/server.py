from flask import Flask, jsonify, request, json, send_file
from flask import render_template
from predict import predict, evaluate, dataset_evaluate
import os
from flask_cors import *
from logging import FileHandler,WARNING

app = Flask(__name__)
CORS(app, supports_credentials=True)
file_handler = FileHandler('errorlog.txt')
file_handler.setLevel(WARNING)

app.config["JSON_AS_ASCII"] =False

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/hello/')
def hello():
    return ("hello world")

@app.route('/predict', methods=['get'])
def predict_text():
    # print("?")
    data = {}
    for key in request.args:
        data[key] = request.args.get(key)
    res = predict(data["orig"], data["cor"])
    return jsonify(res)

@app.route('/upload_predict', methods=['post'])
def upload_predict():
    file = request.files['file']
    file.save("upload_predict.txt")
    return "200"

@app.route('/upload_correct', methods=['post'])
def upload_correct():
    file = request.files['file']
    file.save("upload_correct.txt")
    return "200"

@app.route('/evaluate', methods=['get'])
def evaluate_prediction():
    res = evaluate("upload_predict.txt", "upload_correct.txt")
    # os.remove("upload_predict.txt")
    # os.remove("upload_correct.txt")
    return jsonify(res)

@app.route('/evaluate_example', methods=['get'])
def evaluate_example():
    res = evaluate("../example/predict.txt", "../example/gold.01")
    return jsonify(res)

@app.route('/upload_dataset', methods=['post'])
def upload_dataset():
    file = request.files['file']
    file.save("upload_dataset.txt")
    return "200"

@app.route('/dataset_evaluate', methods=['get'])
def evaluate_dataset():
    res = dataset_evaluate("upload_dataset.txt")
    return jsonify(res)

@app.route('/dataset_evaluate_example', methods=['get'])
def evaluate_dataset_example():
    res = dataset_evaluate("../example/gold.01")
    return jsonify(res)

@app.route('/download_predict', methods=['get'])
def download_predict():
    return send_file("../example/predict.txt", as_attachment=True)

@app.route('/download_dataset', methods=['get'])
def download_dataset():
    return send_file("../example/gold.01", as_attachment=True)

@app.route('/download_dataset2', methods=['get'])
def download_dataset2():
    return send_file("../example/ref.txt", as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
