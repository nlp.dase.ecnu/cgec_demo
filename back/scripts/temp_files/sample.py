import pandas as pd
import random

with open("origin_text.txt", "r", encoding="utf-8") as src_f, open("ref0", "r", encoding="utf-8") as ref_f:
    data = {"src":[],"cor":[]}
    zip_list = [(src_text.strip(), tgt_text.strip()) for (src_text, tgt_text) in zip(src_f.readlines(), ref_f.readlines()) if src_text.strip() != ""]
    sample = random.sample(zip_list, 200)
    src, cor = zip(*sample)
    data["src"] = src
    data["cor"] = cor
    df = pd.DataFrame(data)
    df.to_csv('sample.csv', index=False, header=False, encoding="utf-8-sig")