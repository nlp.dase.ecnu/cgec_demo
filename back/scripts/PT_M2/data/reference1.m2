S 冬阴功 是 泰国 最 著名 的 菜 之一 ， 它 虽然 不 是 很 豪华 ， 但 它 的 味 确实 让 人 上瘾 ， 做法 也 不 难 、 不 复杂 。
A 9 11|||W|||虽然 它|||REQUIRED|||-NONE-|||0

S 首先 ， 我们 得 准备 : 大 虾六 到 九 只 、 盐 一 茶匙 、 已 搾 好 的 柠檬汁 三 汤匙 、 泰国 柠檬 叶三叶 、 柠檬 香草 一 根 、 鱼酱 两 汤匙 、 辣椒 6 粒 ， 纯净 水 4量杯 、 香菜 半量杯 和 草菇 10 个 。
A 17 18|||S|||榨|||REQUIRED|||-NONE-|||0
A 38 39|||S|||六|||REQUIRED|||-NONE-|||0
A 43 44|||S|||四 量杯|||REQUIRED|||-NONE-|||0
A 49 50|||S|||十|||REQUIRED|||-NONE-|||0

S 这样 ， 你 就 会 尝到 泰国人 死爱 的 味道 。
A 7 8|||S|||爱死|||REQUIRED|||-NONE-|||0

S 另外 ， 冬 阴功 对 外国人 的 喜爱 不断 地 增加 。
A 2 6|||W|||外国人 对 冬阴功|||REQUIRED|||-NONE-|||0

S 这 部 电影 不仅 是 国内 ， 在 国外 也 很 有名 。
A 5 5|||M|||在|||REQUIRED|||-NONE-|||0
A 4 5|||S|||在|||REQUIRED|||-NONE-|||1

S 不管 是 真正 的 冬阴功 还是 电影 的 “ 冬阴功 ” ， 都 在 人们 的 心 里 刻骨 铭心 。
A 12 18|||S|||人们 都|||REQUIRED|||-NONE-|||0
A 7 7|||M|||中|||REQUIRED|||-NONE-|||1
A 13 18|||S|||使 人们|||REQUIRED|||-NONE-|||1

S 随着 中国 经济 突飞猛 近 ， 建造 工业 与日俱增 。
A 3 5|||S|||突飞猛进|||REQUIRED|||-NONE-|||0

S 虽然 工业 的 发展 和 城市 规模 的 扩大 对 经济 发展 有 积极 作用 ， 但是 同时 也 对 环境 问题 日益 严重 造成 了 空气 污染 问题 。
A 21 22|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 22 26|||W|||造成 了 日益 严重 的|||REQUIRED|||-NONE-|||0
A 21 22|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 22 26|||W|||造成 了 日益 严重|||REQUIRED|||-NONE-|||1
A 26 27|||M|||的|||REQUIRED|||-NONE-|||1

S 那些 空气 污染 也 没 有助于 人生 的 身体 建康 。
A 4 6|||S|||无助于|||REQUIRED|||-NONE-|||0
A 6 7|||S|||人|||REQUIRED|||-NONE-|||0
A 9 10|||S|||健康|||REQUIRED|||-NONE-|||0

S 由此 可 见 ， 首先 我们 要 了解 一些 关于 空气 污染 对 我们 人生 有 什么 危害 的 话题 知道 了 这些 常识 以后 对 我们 人类 会 有 积极 作用 。 以及 要 学会 怎样 应对 和 治理 空气 污染 的 问题 。
A 14 15|||S|||人体|||REQUIRED|||-NONE-|||0
A 20 20|||M|||——|||REQUIRED|||-NONE-|||0
A 21 22|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 24 25|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 32 33|||S|||，|||REQUIRED|||-NONE-|||0
A 14 15|||S|||人体|||REQUIRED|||-NONE-|||1
A 20 20|||M|||——|||REQUIRED|||-NONE-|||1
A 21 22|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 24 25|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 32 33|||S|||，|||REQUIRED|||-NONE-|||1
A 33 34|||S|||其次|||REQUIRED|||-NONE-|||1

S 任何 事情 都 是 各 有 利弊 ， 众所周知 越 建立 工业 越 对 经济 方面 有所 发展 。
A 10 11|||S|||发展|||REQUIRED|||-NONE-|||0
A 12 14|||S|||，|||REQUIRED|||-NONE-|||0
A 16 16|||M|||就 越|||REQUIRED|||-NONE-|||0
A 7 7|||M|||的|||REQUIRED|||-NONE-|||1
A 10 11|||S|||发展|||REQUIRED|||-NONE-|||1
A 12 14|||S|||，|||REQUIRED|||-NONE-|||1
A 16 16|||M|||就 越|||REQUIRED|||-NONE-|||1

S 对 我 看来 ， 曼 古 空气 污染 的 问题 与日俱增 。
A 0 1|||S|||在|||REQUIRED|||-NONE-|||0

S 每 天 会 有 不少 的 毒气体 泄漏 从 工厂 里 出来 。
A 6 7|||S|||有毒 气体|||REQUIRED|||-NONE-|||0
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 11 11|||M|||泄漏|||REQUIRED|||-NONE-|||0
A 6 7|||S|||有毒 气体|||REQUIRED|||-NONE-|||1
A 7 11|||W|||从 工厂 里 泄露|||REQUIRED|||-NONE-|||1

S 在 工厂 里 的 工作 人员 为了 工作 ， 而 每 天 吸 了 不少 的 毒气体 ， 经过 了 一 年 多 的 时间 ， 连 工作 人员 也 得 了 严重 的 病 。 更 不用 说 住 在 这 家 工厂 近 的 家庭 。
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 16 17|||S|||有毒 气体|||REQUIRED|||-NONE-|||0
A 35 36|||S|||，|||REQUIRED|||-NONE-|||0
A 40 41|||S|||得 离|||REQUIRED|||-NONE-|||0
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 16 17|||S|||有毒 气体|||REQUIRED|||-NONE-|||1
A 35 36|||S|||，|||REQUIRED|||-NONE-|||1
A 44 45|||S|||附近|||REQUIRED|||-NONE-|||1

S 沙尘暴 也 是 一 类 空气 污染 之一 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 3 5|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 不 官 是 从 口 、 眼 、 鼻子 进去 这样 会 伤害 身体 的 建康 。
A 0 2|||S|||不管|||REQUIRED|||-NONE-|||0
A 7 8|||S|||还是|||REQUIRED|||-NONE-|||0
A 10 11|||S|||， 都|||REQUIRED|||-NONE-|||0
A 15 16|||S|||健康|||REQUIRED|||-NONE-|||0

S 这样 做 会 避免 受到 沙尘暴 。
A 6 6|||M|||的 危害|||REQUIRED|||-NONE-|||0

S 最后 ， 要 关主 一些 关于 天气 预报 的 新闻 。
A 3 4|||S|||关注|||REQUIRED|||-NONE-|||0

S 中国 ， 悠久 的 历史 ， 灿烂 的 文化 ， 真 是 在 历史 上 最 难忘 的 国家 。
A 2 2|||M|||拥有|||REQUIRED|||-NONE-|||0
A 15 15|||M|||让 人|||REQUIRED|||-NONE-|||0

S 对 一 个 生 名 来 说 空气 污染 是 很 危害 的 问题 ， 对 身体 不 好 。
A 3 5|||S|||生命|||REQUIRED|||-NONE-|||0
A 10 10|||M|||有|||REQUIRED|||-NONE-|||0
A 11 11|||M|||大|||REQUIRED|||-NONE-|||0
A 3 5|||S|||生命|||REQUIRED|||-NONE-|||1
A 11 11|||M|||有|||REQUIRED|||-NONE-|||1
A 13 14|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 抽烟 对 身体 非常 不 好 ， 对 身体 非常 危害 。
A 9 11|||W|||危害 非常 大|||REQUIRED|||-NONE-|||0

S 在 中国 ， 不 官 是 谁 ， 都 抽烟 。
A 3 5|||S|||不管|||REQUIRED|||-NONE-|||0

S 但是 医生 说 ， 他 的 身体 是 跟 抽烟 的 人 完全 一样 ， 好像 抽烟 抽 得 六十 年 一样 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 18 19|||S|||了|||REQUIRED|||-NONE-|||0

S 在 证治 方面 空气 污染 也 很 不 好 。
A 1 2|||S|||政治|||REQUIRED|||-NONE-|||0

S 现在 人们 会 认为 中国 ， 持 别 是 北京 ， 没有 “ 自然 ” 的 感觉 。
A 6 7|||S|||特|||REQUIRED|||-NONE-|||0

S 例如 ， 天气 非常 好 的 时候 比 天气 非常 不 好 的 时候 完全 不 一样 。 天气 好 时 大部分 的 人 会 高兴 。
A 7 8|||S|||和|||REQUIRED|||-NONE-|||0
A 17 18|||S|||:|||REQUIRED|||-NONE-|||0

S 工业 发展 是 好 东西 ， 但是 好 的 东西 也 有 不 好 的 东西 。
A 15 16|||S|||方面|||REQUIRED|||-NONE-|||0
A 15 16|||S|||一面|||REQUIRED|||-NONE-|||1

S 工业 发展 把 中国 当 个 很 强 的 国家 。
A 4 5|||S|||变成 一|||REQUIRED|||-NONE-|||0

S 北京 的 空气 太 污染 了 ， 泛 在 北京 的 人 一定 要 注意 ， 别 抽烟 。
A 3 3|||M|||污染|||REQUIRED|||-NONE-|||0
A 4 5|||S|||严重|||REQUIRED|||-NONE-|||0
A 7 8|||S|||凡|||REQUIRED|||-NONE-|||0

S 大家 希望 裕富 的 生活 。
A 2 3|||S|||过上 富裕|||REQUIRED|||-NONE-|||0

S 我 认为 空气 污染 是 跟 我们 的 生活 密切 的 问题 ， 所以 一定 要 最 优先 解决 ， 优 其 是 像 北京 那样 的 大 城市 。
A 10 10|||M|||相关|||REQUIRED|||-NONE-|||0
A 20 21|||S|||尤|||REQUIRED|||-NONE-|||0

S 林冲一路 来到 了 李小二 的 店 里 ， 选 了 一 张 比较 隐避 的 桌子 坐 了 下来 。
A 13 14|||S|||隐蔽|||REQUIRED|||-NONE-|||0

S 现代 社会 的 就业 难 太 厉害 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 6 7|||S|||难|||REQUIRED|||-NONE-|||0

S 因为 读书 准备 就业 的 时间 越 少 了 。
A 2 2|||M|||，|||REQUIRED|||-NONE-|||0
A 6 7|||S|||更|||REQUIRED|||-NONE-|||0

S 但是 这 种 想法 太 近视眼 的 ， 而 有 很 大 的 错误 。
A 5 6|||S|||短浅|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 8 9|||S|||而且|||REQUIRED|||-NONE-|||0
A 5 6|||S|||目光 短浅|||REQUIRED|||-NONE-|||1
A 6 7|||S|||了|||REQUIRED|||-NONE-|||1
A 8 9|||S|||而且|||REQUIRED|||-NONE-|||1

S 一般 人 觉得 只 有 流利 的 英语 实力 、 好多 征 件 、 丰富 的 活动 经历 什么 的 人 才 能 找到 好 工作 。
A 5 5|||M|||拥有|||REQUIRED|||-NONE-|||0
A 5 6|||S|||强大|||REQUIRED|||-NONE-|||0
A 11 13|||S|||证件|||REQUIRED|||-NONE-|||0
A 18 19|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 5 5|||M|||拥有|||REQUIRED|||-NONE-|||1
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 11 13|||S|||证件|||REQUIRED|||-NONE-|||1
A 18 19|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 如果 读 各种各样 的 书 ， 能 培养 各种各样 的 能力 ， 变得 更 创新 而 思考 的 深度 更 深 。 那 多么 公司 会 需要 录用 他 。
A 6 6|||M|||就|||REQUIRED|||-NONE-|||0
A 19 19|||M|||会|||REQUIRED|||-NONE-|||0
A 21 22|||S|||，|||REQUIRED|||-NONE-|||0
A 22 24|||S|||那么|||REQUIRED|||-NONE-|||0
A 26 27|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 15 16|||S|||而且|||REQUIRED|||-NONE-|||1
A 21 22|||S|||，|||REQUIRED|||-NONE-|||1
A 22 24|||S|||那么|||REQUIRED|||-NONE-|||1

S 大学 是 让 学生们 能 作为 一 名 好 成人 ， 好 的 社会 的 一 部分 。
A 1 1|||M|||的 目的|||REQUIRED|||-NONE-|||0
A 5 6|||S|||成为|||REQUIRED|||-NONE-|||0
A 11 15|||M|||社会 的 好 的|||REQUIRED|||-NONE-|||0
A 1 1|||M|||的 目的|||REQUIRED|||-NONE-|||1
A 5 6|||S|||成为|||REQUIRED|||-NONE-|||1
A 9 9|||M|||的|||REQUIRED|||-NONE-|||1
A 11 15|||W|||社会 的 好 的|||REQUIRED|||-NONE-|||1

S 不管 对 就业 有 没 有 直接 的 利益 ， 学生们 要 认真 学习 。
A 11 11|||M|||都|||REQUIRED|||-NONE-|||0

S 当然 现在 的 情况 下 ， 在 大学 学生们 只 读书 是 难 的 事情 。 准备 找 工作 也 重要 极 了 。
A 1 1|||M|||在|||REQUIRED|||-NONE-|||0
A 15 16|||S|||，|||REQUIRED|||-NONE-|||0

S 近来 就业 难 是 一 个 社会 问题 。 大学 毕业生 也 不 是 外 。
A 8 9|||S|||，|||REQUIRED|||-NONE-|||0
A 14 15|||S|||例外|||REQUIRED|||-NONE-|||0
A 8 9|||S|||，|||REQUIRED|||-NONE-|||1
A 13 15|||S|||例外|||REQUIRED|||-NONE-|||1

S 所以 一些 人 说 ， : 读书 一点 用处 都 没有 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 他们 的 主张 里 的 最 大 的 错误 在 这里 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 学习 是 以 得到 智慧 ， 不 是 以 就业 。
A 10 10|||M|||为 目的|||REQUIRED|||-NONE-|||0
A 5 5|||M|||为 目的|||REQUIRED|||-NONE-|||1
A 10 10|||M|||为 目的|||REQUIRED|||-NONE-|||1

S 社会 越来越 成 专业化 、 灵通化 。
A 2 3|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 照 此 趋势 类推 ， 将来 更 多 的 人 被 机器 掠夺 了 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 10 10|||M|||会|||REQUIRED|||-NONE-|||0
A 13 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 现代 的 科学 技术 发展 已经 能 代 人 在 那 种 作业 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 9 10|||S|||进行|||REQUIRED|||-NONE-|||0
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 12 12|||M|||环境 下|||REQUIRED|||-NONE-|||1

S 以 找到 这些 稳定 的 工作 ， 我们 有 读书 的 必要 。
A 0 1|||S|||为了|||REQUIRED|||-NONE-|||0

S 读 一 本 书 让 你 可能 看 另外 的 角度 ， 而且 获得 宝贝 的 知识 。
A 4 4|||M|||可能|||REQUIRED|||-NONE-|||0
A 6 8|||S|||拥有|||REQUIRED|||-NONE-|||0
A 13 13|||M|||让 你|||REQUIRED|||-NONE-|||0
A 14 15|||S|||宝贵|||REQUIRED|||-NONE-|||0
A 6 8|||S|||能 从|||REQUIRED|||-NONE-|||1
A 11 11|||M|||看 问题|||REQUIRED|||-NONE-|||1
A 13 13|||M|||让 你|||REQUIRED|||-NONE-|||1
A 14 15|||S|||宝贵|||REQUIRED|||-NONE-|||1

S 读 很多 书 让 我们 明理 ， 并且 可 会 有 逻辑 思维 。
A 8 11|||S|||拥有|||REQUIRED|||-NONE-|||0

S 总是 面试 非常 重要 。
A 0 1|||S|||总之|||REQUIRED|||-NONE-|||0

S 你 读 很多 书 的话 ， 你 可以 成功 。
A 6 7|||S|||就|||REQUIRED|||-NONE-|||0

S 可 现在 有 很多 大学 ， 大部分 人 几乎 上 大学 。
A 8 9|||S|||都|||REQUIRED|||-NONE-|||0
A 8 9|||S|||都 能|||REQUIRED|||-NONE-|||1

S 这样 的 事情 让 政府 感到 负担 。
A 6 6|||M|||有|||REQUIRED|||-NONE-|||0
A 6 7|||S|||担忧|||REQUIRED|||-NONE-|||1

S 找 工作 的 人 比 工司 更 多 。
A 5 6|||S|||公司|||REQUIRED|||-NONE-|||0

S 我 的 家 附近 有 很多 考式 补习班 。
A 6 7|||S|||考试|||REQUIRED|||-NONE-|||0

S 如果 结果 好 的话 ， 没 关系 。 可 不 好 的话 ， 浪费 时间 。
A 7 8|||S|||，|||REQUIRED|||-NONE-|||0
A 13 13|||M|||这 会|||REQUIRED|||-NONE-|||0
A 7 8|||S|||，|||REQUIRED|||-NONE-|||1
A 13 13|||M|||就 是|||REQUIRED|||-NONE-|||1

S 最近 高中生 比 有名 的 大学 更 喜欢 技术 大学 。
A 0 6|||W|||比起 有名 的 大学 ， 最近 高中生|||REQUIRED|||-NONE-|||0

S 因为 这 个 问题 ， 所以 最近 头疼 。
A 6 6|||M|||我|||REQUIRED|||-NONE-|||0

S 大学 的 功能 不 是 这样 的 。 就 是 学 的 。
A 7 8|||S|||，|||REQUIRED|||-NONE-|||0
A 8 9|||S|||而|||REQUIRED|||-NONE-|||0
A 10 10|||M|||用 来|||REQUIRED|||-NONE-|||0

S 我 非常 荣幸 能够 选修 这 门 课程 ， 这 个 班级 。
A 8 9|||S|||、 加入|||REQUIRED|||-NONE-|||0
A 9 9|||M|||加入|||REQUIRED|||-NONE-|||1

S 这 学期 学到 了 多 种 形式 的 文章 ， 从 日常 书信 、 专用 书信 、 便条 、 字据 、 说明文 和 总结 等 。
A 2 2|||M|||我|||REQUIRED|||-NONE-|||0
A 9 11|||S|||:|||REQUIRED|||-NONE-|||0
A 2 2|||M|||我|||REQUIRED|||-NONE-|||1
A 22 23|||S|||到|||REQUIRED|||-NONE-|||1

S 从 学习 写 日常 书信 和 专用 书信 的 课 中 ， 我 毕到 了 如何 在 不同 场合 使用 不同 语言 的 方式 。
A 13 14|||S|||学到|||REQUIRED|||-NONE-|||0
A 21 21|||M|||的|||REQUIRED|||-NONE-|||0
A 22 23|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 我 学到 了 如 和 更 简练 ， 更 书面 地 表达 自己 的 意见 。
A 3 5|||S|||如何|||REQUIRED|||-NONE-|||0
A 7 8|||S|||、|||REQUIRED|||-NONE-|||0

S 我 学到 了 为 自己 的 过去 进行 歹思 并 鼓起 勇气 的 方法 。
A 8 9|||S|||反思|||REQUIRED|||-NONE-|||0

S 阅读 与 写作 是 在 华 留学生 必修 的 一 门 课 ， 有 它 ， 让 我们 的 留学 生活 更 充实 ， 更 完美 。
A 16 17|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 23 24|||S|||、|||REQUIRED|||-NONE-|||0
A 13 16|||S|||它|||REQUIRED|||-NONE-|||1
A 23 24|||S|||、|||REQUIRED|||-NONE-|||1

S 因为 几乎 的 人们 还 没 感到 污染 对 自己 的 直接 的 影响 。
A 2 2|||M|||所有|||REQUIRED|||-NONE-|||0

S 空气 虽然 不 可 视 的 物质 ， 但是 生活 中 不 可 缺少 的 特质 。
A 2 2|||M|||是|||REQUIRED|||-NONE-|||0
A 4 5|||S|||见|||REQUIRED|||-NONE-|||0
A 9 9|||M|||在|||REQUIRED|||-NONE-|||0
A 11 11|||M|||却 是|||REQUIRED|||-NONE-|||0
A 15 16|||S|||物质|||REQUIRED|||-NONE-|||0
A 2 2|||M|||是|||REQUIRED|||-NONE-|||1
A 4 5|||S|||见|||REQUIRED|||-NONE-|||1
A 9 9|||M|||却 是|||REQUIRED|||-NONE-|||1
A 15 16|||S|||物质|||REQUIRED|||-NONE-|||1

S 尊 守 一 个 一 个 ， 自己 制定 的 小 活动 。
A 0 2|||S|||开展|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 0 2|||S|||遵守|||REQUIRED|||-NONE-|||1
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 11 12|||S|||计划|||REQUIRED|||-NONE-|||1

S 近代 木柴 被 认为 重要 的 资原 、 能 原 ， 因此 被 用 得 越来越 多 。
A 1 1|||M|||以来|||REQUIRED|||-NONE-|||0
A 4 4|||M|||是|||REQUIRED|||-NONE-|||0
A 6 7|||S|||资源|||REQUIRED|||-NONE-|||0
A 8 10|||S|||能源|||REQUIRED|||-NONE-|||0

S 每 年 被 用 的 树木 的 数 比 植 的 树木 非常 多 。
A 3 4|||S|||使用|||REQUIRED|||-NONE-|||0
A 7 8|||S|||数量|||REQUIRED|||-NONE-|||0
A 9 10|||S|||种植|||REQUIRED|||-NONE-|||0
A 12 13|||S|||多得|||REQUIRED|||-NONE-|||0

S 树 越 多 就 恢复 污染 空气 的 速度 越 快 。
A 3 7|||W|||空气 恢复 洁净|||REQUIRED|||-NONE-|||0
A 3 9|||W|||空气 恢复 洁净 的 速度 就|||REQUIRED|||-NONE-|||1

S 列车 、 汽车 ， 飞机 等 人类 科技 发展 的 结果 也 重大 问题 。
A 3 4|||S|||、|||REQUIRED|||-NONE-|||0
A 12 12|||M|||存在|||REQUIRED|||-NONE-|||0

S 1 . 少 骑 汽车 ; 2.多 用 公共 施设 ， 如 有 地铁 、 公共 汽车 等 ; 3 . 多 走步 多 骑 自行车 。
A 3 4|||S|||开|||REQUIRED|||-NONE-|||0
A 9 10|||S|||设施|||REQUIRED|||-NONE-|||0
A 12 13|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 22 23|||S|||步行 、|||REQUIRED|||-NONE-|||0

S 导 到 空气 污染 的 代表 要素 是 汽车 的 尾气 。
A 0 2|||S|||导致|||REQUIRED|||-NONE-|||0
A 6 7|||S|||因素|||REQUIRED|||-NONE-|||0

S 不仅 学会 了 很多 知识 ， 而且 发现 对 自己 合适 ， 有效 的 学习 方法 。
A 8 8|||M|||了|||REQUIRED|||-NONE-|||0
A 11 12|||S|||、|||REQUIRED|||-NONE-|||0

S 这些 问题 其实 是 不 该 范 的 ， 写 文章 再 要 细心 ， 重视 细节 。
A 1 2|||S|||错误|||REQUIRED|||-NONE-|||0
A 6 7|||S|||犯|||REQUIRED|||-NONE-|||0
A 11 12|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 14 15|||S|||、|||REQUIRED|||-NONE-|||0

S 总的来说 ， 这 个 学期 的 学习 情况 较 满意 。
A 2 2|||M|||我 对|||REQUIRED|||-NONE-|||0

S 中国 近 几十 年 来 的 发展 带来 了 很多 大 问题 ， 最 大 的 可以 说 是 环境 上 的 几 样 ， 特别 取得 关注 的 是 空气 污染 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 26 27|||S|||值得|||REQUIRED|||-NONE-|||0

S 空气 污染 的 问题 已 变成 一 个 世界 上 共 存在 的 问题 之一 。
A 10 11|||S|||共同|||REQUIRED|||-NONE-|||0
A 14 15|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 6 8|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 10 11|||S|||共同|||REQUIRED|||-NONE-|||1

S 我们 应该 怎么 去 治理 和 保护 自己 ？ 先 需要 知 到 空气 污染 是 什么 。
A 5 5|||M|||环境|||REQUIRED|||-NONE-|||0
A 9 10|||S|||首先|||REQUIRED|||-NONE-|||0
A 11 13|||S|||知道|||REQUIRED|||-NONE-|||0

S 空气 污染 已经 对 晋同 公民 有 一定 的 危害 了 。
A 4 5|||S|||普通|||REQUIRED|||-NONE-|||0

S 大使 管所 查到 的 都 可以 上网 ， 或者 下 一 个 应用 去 自己 看 。
A 0 2|||S|||大使馆所|||REQUIRED|||-NONE-|||0
A 4 4|||M|||数据 我们|||REQUIRED|||-NONE-|||0
A 13 15|||W|||自己 去|||REQUIRED|||-NONE-|||0

S 如果 全 世界 多 注意 ， 可能 有 一 天 我们 就 不 用 出门 害怕 空气 污染 了 。
A 3 4|||S|||都|||REQUIRED|||-NONE-|||0
A 11 15|||W|||出门 就 不 用|||REQUIRED|||-NONE-|||0

S 其 结果 全 世界 的 环境 问题 越 严重 。
A 2 2|||M|||是|||REQUIRED|||-NONE-|||0
A 7 8|||S|||越来越|||REQUIRED|||-NONE-|||0

S 空气 污染 后 受害 的 不 是 空气 ， 而 是 我们 人类 受害 。
A 13 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 我们 吸受 污染 的 空气 ， 会 致癌 。
A 1 2|||S|||吸收|||REQUIRED|||-NONE-|||0
A 7 8|||S|||得 癌症|||REQUIRED|||-NONE-|||0
A 1 2|||S|||呼吸|||REQUIRED|||-NONE-|||1
A 7 8|||S|||得 癌症|||REQUIRED|||-NONE-|||1

S 可以 把 污染 的 空气 变成 为 清晰 的 空气 。
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 7 8|||S|||洁净|||REQUIRED|||-NONE-|||0

S 工厂 的 排放 导致 雾霾 ， 所以 政府 主动 地 控制 工厂 的 排放量 。
A 1 2|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 3 3|||M|||的 气体|||REQUIRED|||-NONE-|||0
A 8 8|||M|||应该|||REQUIRED|||-NONE-|||0

S 空气 污染 的 危害性 是 不 用 再说 的 ， 应该 没 个人 都 知道 。
A 7 8|||S|||多 说|||REQUIRED|||-NONE-|||0
A 11 12|||S|||每|||REQUIRED|||-NONE-|||0

S 为什么 我 这么 说 呢 ？ 因为 全 世界 的 人们 都 生活 在 地球 上 ， 要是 地球 因为 环境 污染 所以 受到 很 大 的 冲击 的话 ， 就 影响 到 在 地球 上 生活 的 人 。
A 22 23|||S|||而|||REQUIRED|||-NONE-|||0
A 31 31|||M|||会|||REQUIRED|||-NONE-|||0
A 22 23|||S|||而|||REQUIRED|||-NONE-|||1
A 27 28|||S|||影响|||REQUIRED|||-NONE-|||1
A 31 31|||M|||会|||REQUIRED|||-NONE-|||1

S 但是 不 能 因为 因此 停滞不前 或 退却 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 5 5|||M|||而|||REQUIRED|||-NONE-|||0
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 人为 了 生存 不管是 干静 的 空气 ， 污染 的 空气 都 要 呼吸 。
A 4 5|||S|||干净|||REQUIRED|||-NONE-|||0
A 8 8|||M|||还是|||REQUIRED|||-NONE-|||0
A 11 11|||M|||，|||REQUIRED|||-NONE-|||0
A 3 3|||M|||，|||REQUIRED|||-NONE-|||1
A 4 5|||S|||干净|||REQUIRED|||-NONE-|||1
A 8 8|||M|||还是|||REQUIRED|||-NONE-|||1
A 11 11|||M|||，|||REQUIRED|||-NONE-|||1

S 空气 污染 会 景响 人们 的 生活 质量 和 过 幸福 的 生活 的 权力 。
A 3 4|||S|||影响|||REQUIRED|||-NONE-|||0
A 9 9|||M|||剥夺 人们|||REQUIRED|||-NONE-|||0
A 14 15|||S|||权利|||REQUIRED|||-NONE-|||0

S 空气 污染 不但 带来 建康 的 问题 ， 会 带来 物质 上 的 问题 。
A 4 5|||S|||健康 上|||REQUIRED|||-NONE-|||0
A 8 8|||M|||还|||REQUIRED|||-NONE-|||0

S 车 的 尾气 是 空气 污染 的 罪愧 元首 。
A 7 9|||S|||罪魁祸首|||REQUIRED|||-NONE-|||0

S 林冲 觉得 困 ， 很 累 ， 想 吃 点 东西 ， 喝点酒 。
A 3 4|||S|||、|||REQUIRED|||-NONE-|||0
A 11 12|||S|||、|||REQUIRED|||-NONE-|||0

S 我 就 在 此 了解 了 你 这 罪魁祸手 ， 平平 风气 ！
A 4 5|||S|||了结|||REQUIRED|||-NONE-|||0
A 8 9|||S|||罪魁祸首|||REQUIRED|||-NONE-|||0

S 林冲想 把 这些 头 回京 ， 府 里 见 太 尉时 给 他 。
A 4 4|||M|||带|||REQUIRED|||-NONE-|||0
A 6 6|||M|||在|||REQUIRED|||-NONE-|||0

S 有 一 天 晚上 他 下 了 决定 向 富丽 堂皇 的 宫殿 里 走 ， 偷偷 的 进入 宫内 。
A 7 8|||S|||决心|||REQUIRED|||-NONE-|||0
A 17 18|||S|||地|||REQUIRED|||-NONE-|||0

S 我 的 人生 怎么 这么 残 呢 ?
A 5 6|||S|||惨|||REQUIRED|||-NONE-|||0

S 林冲因 富安 、 陆谦 和 差拨 三 个 人 无法 容忍 的 背叛 行为 ， 仿佛 是 他们 三 个 在 背后 捅 了 他 一 刀 一样 ， 一 想起 山 神庙 的 事 气 得 直达 牙关 。
A 15 15|||M|||感到|||REQUIRED|||-NONE-|||0
A 37 38|||S|||直咬|||REQUIRED|||-NONE-|||0

S 官吏 腐败 ， 尤其 几 个 贪官 污吏 老是 枪 夺 人民 的 财产 ， 百姓 生活 更加 贫穷 。
A 9 11|||S|||抢夺|||REQUIRED|||-NONE-|||0
A 15 15|||M|||使得|||REQUIRED|||-NONE-|||0

S 我 转学 的 时候 ， 感到 痛苦 ， 之所以 转学 以为 着 失去 我 在 学校 的 一切 美丽 时光 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 8 9|||S|||因为|||REQUIRED|||-NONE-|||0
A 10 11|||S|||意味|||REQUIRED|||-NONE-|||0

S 刚 到 康桥 中学 时 ， 我 眼前 的 情景 是 韩国 留学生 的 上课 拒绝 。
A 13 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 14 16|||W|||拒绝 上课|||REQUIRED|||-NONE-|||0

S 有 一 天 ， 他们 终于 让 善良 的 女 老师 哭 而 跑出 教室 。
A 12 13|||S|||着|||REQUIRED|||-NONE-|||0

S 虽然 我 是 无能 的 ， 而且 喜欢 玩 电脑 的 笨猪 ， 但是 眼前 的 场景 是 不 可 容纳 的 ， 也 不 可 允许 的 。
A 5 6|||S|||、|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 20 21|||S|||容忍|||REQUIRED|||-NONE-|||0
A 22 23|||S|||、|||REQUIRED|||-NONE-|||0
A 5 6|||S|||、|||REQUIRED|||-NONE-|||1
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 20 21|||S|||容忍|||REQUIRED|||-NONE-|||1
A 22 23|||S|||、|||REQUIRED|||-NONE-|||1
A 25 26|||S|||被|||REQUIRED|||-NONE-|||1

S 然后 跑出 教室 安慰 那 位 女 老师 以及 所有 的 情况 告诉 班主任 老师 。
A 1 1|||M|||我|||REQUIRED|||-NONE-|||0
A 9 9|||M|||把|||REQUIRED|||-NONE-|||0
A 1 1|||M|||我|||REQUIRED|||-NONE-|||1
A 9 9|||M|||把|||REQUIRED|||-NONE-|||1
A 13 13|||M|||了|||REQUIRED|||-NONE-|||1

S 我 考上 北大 之后 遇然 听到 欺负 我 的 那些 留学生 的 近况 。
A 4 5|||S|||偶然|||REQUIRED|||-NONE-|||0

S 可 它 的 表情 是 从来 没 看 过 的 。
A 5 5|||M|||我|||REQUIRED|||-NONE-|||0

S 我 把 弟弟 醒 过来 后 ， 一起 开始 找 答案 。
A 3 5|||S|||叫醒|||REQUIRED|||-NONE-|||0

S 从此 以后 ， 我 再 也 不 撒荒 、 抄 答案 等等 行为 。
A 7 8|||S|||撒谎|||REQUIRED|||-NONE-|||0
A 12 13|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 那 天 ， 我们 要 做 比 原来 两 倍 的 作业 。
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 6 7|||S|||相当于|||REQUIRED|||-NONE-|||1

S 高中时 我们 穿 朴素 端正 的 校服 ， 不 需 华丽 的 装饰 那时 我们 只 报 一 个 书包 就 幸福 了 。
A 9 10|||S|||需要|||REQUIRED|||-NONE-|||0
A 13 13|||M|||，|||REQUIRED|||-NONE-|||0
A 16 17|||S|||背|||REQUIRED|||-NONE-|||0
A 9 10|||S|||需要|||REQUIRED|||-NONE-|||1
A 13 13|||M|||，|||REQUIRED|||-NONE-|||1
A 16 17|||S|||背|||REQUIRED|||-NONE-|||1

S 我 高中 生活 的 滋味 曼延 我 全身 ， 意 犹 末 尽 ， 沁 人心脾 。
A 5 6|||S|||蔓延|||REQUIRED|||-NONE-|||0
A 9 13|||S|||意犹未尽|||REQUIRED|||-NONE-|||0
A 13 14|||S|||、|||REQUIRED|||-NONE-|||0

S 有 为 之 没 走进 唱片 行 了 ？ 有 为 之 没 感受 到 当初 的 那 种 悸动 了 呢 ？
A 1 3|||S|||多久|||REQUIRED|||-NONE-|||0
A 10 12|||S|||多久|||REQUIRED|||-NONE-|||0

S 从 我 个人 而 言 ， 在 一 个 城市 里 ， 文物 和 城市 发展 这 两 个 都 非常 的 重要 。
A 0 1|||S|||对|||REQUIRED|||-NONE-|||0
A 18 19|||S|||者|||REQUIRED|||-NONE-|||0
A 0 1|||S|||对|||REQUIRED|||-NONE-|||1
A 18 19|||S|||点|||REQUIRED|||-NONE-|||1

S 日本 的 京都 奈良 、 历史 虽然 比 不 上 西安 、 但是 从 建都 以来 的 重要 文物 几乎 全部 保护 到 现在 。
A 3 3|||M|||、|||REQUIRED|||-NONE-|||0
A 4 5|||S|||，|||REQUIRED|||-NONE-|||0
A 11 12|||S|||，|||REQUIRED|||-NONE-|||0
A 21 21|||M|||都 被|||REQUIRED|||-NONE-|||0
A 23 23|||M|||了|||REQUIRED|||-NONE-|||0
A 3 3|||M|||、|||REQUIRED|||-NONE-|||1
A 4 5|||S|||，|||REQUIRED|||-NONE-|||1
A 11 12|||S|||，|||REQUIRED|||-NONE-|||1
A 21 22|||S|||保存|||REQUIRED|||-NONE-|||1
A 23 23|||M|||了|||REQUIRED|||-NONE-|||1

S 城市 历史 文化 保护 关系 到 城市 的 文化 品位 、 知名度 ， 总合 竞争力 ， 是 一 个 重要 的 课题 。
A 12 13|||S|||、|||REQUIRED|||-NONE-|||0
A 13 14|||S|||综合|||REQUIRED|||-NONE-|||0

S 但是 ， 互联网 对 我们 有利 还是 有 弊 都 由 我们 如何 使用 。
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 10 11|||S|||取决于|||REQUIRED|||-NONE-|||0
A 14 14|||M|||它|||REQUIRED|||-NONE-|||0

S 不 知道 的 知识 我们 会 赶快 能 找到 。
A 5 6|||S|||能 很|||REQUIRED|||-NONE-|||0
A 6 7|||S|||快|||REQUIRED|||-NONE-|||0
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 我 的 父亲 在 染色 技术 研究所 的 职员 。
A 3 4|||S|||是|||REQUIRED|||-NONE-|||0

S 她 已经 毕业 了 大学 ， 专业 是 视觉 设计 。
A 2 2|||M|||从 大学|||REQUIRED|||-NONE-|||0
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 2 6|||W|||大学 毕业 了|||REQUIRED|||-NONE-|||1

S 学 外语 是 很 难得 事情 ， 尚且 汉语 有 许多 汉字 也 有 古代 使用 的 汉字 。
A 3 3|||M|||件|||REQUIRED|||-NONE-|||0
A 4 5|||S|||难 的|||REQUIRED|||-NONE-|||0
A 7 8|||S|||而且|||REQUIRED|||-NONE-|||0
A 11 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 时间 过 的 很 快 ， 我 已经 变成 了 三 年级 。
A 2 3|||S|||得|||REQUIRED|||-NONE-|||0
A 12 12|||M|||的 学生|||REQUIRED|||-NONE-|||0

S 有 可能 ， 因为 我 的 生活 很 忙 所以 有 感觉 时间 过 得 快 。
A 1 2|||S|||时候|||REQUIRED|||-NONE-|||0
A 10 11|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 15 15|||M|||很|||REQUIRED|||-NONE-|||0

S 学习 汉语 的 很多 外国 学生 在 写作 汉语 时 ， 伤 了 不少 脑筋 。
A 0 4|||M|||很多 学习 汉语 的|||REQUIRED|||-NONE-|||0
A 0 4|||W|||很多 学习 汉语 的|||REQUIRED|||-NONE-|||1

S 阳光 灿烂 的 今天 我 走进 了 清华 大学 的 校园 。
A 0 0|||M|||今天|||REQUIRED|||-NONE-|||0
A 2 4|||S|||，|||REQUIRED|||-NONE-|||0
A 0 0|||M|||在|||REQUIRED|||-NONE-|||1
A 4 4|||M|||，|||REQUIRED|||-NONE-|||1

S 它们 的 美 逐渐 使 我 所 迷住 。
A 4 5|||S|||把|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 如果 我 遇到 困难 ， 他们 帮助 我 、 关照 我 。
A 6 6|||M|||会|||REQUIRED|||-NONE-|||0

S 从 那天 开始 ， 我们 每 天 在 北语 哪 咖啡厅 一起 看 电视剧 。
A 7 7|||M|||都|||REQUIRED|||-NONE-|||0
A 9 10|||S|||那个|||REQUIRED|||-NONE-|||0
A 7 7|||M|||都|||REQUIRED|||-NONE-|||1
A 9 10|||S|||那个|||REQUIRED|||-NONE-|||1

S 我 推荐 你 也 每 天 看 一二节 的 电视剧 ， 对 学 韩语 的 口语 、 听力 肯定 有 大 进步 ！
A 7 8|||S|||一二 集|||REQUIRED|||-NONE-|||0
A 20 21|||S|||很大|||REQUIRED|||-NONE-|||0
A 21 22|||S|||帮助|||REQUIRED|||-NONE-|||0

S 我 一 个 人 在 云南 旅行 当中 ， 和 你 认识 的 事 是 很 幸福 的 。
A 7 8|||S|||时|||REQUIRED|||-NONE-|||0
A 9 9|||M|||能|||REQUIRED|||-NONE-|||0
A 12 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 15 15|||M|||件|||REQUIRED|||-NONE-|||0
A 18 18|||M|||事|||REQUIRED|||-NONE-|||0
A 7 8|||S|||时|||REQUIRED|||-NONE-|||1
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 10 12|||W|||认识 你|||REQUIRED|||-NONE-|||1
A 12 15|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 17 18|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 我 一 个 人 看 明 胜 古迹 的 时候 ， 你 先 对 我 说话 ， 而且 你 就 介绍 和 你 亲密 的 朋友们 。
A 5 7|||S|||名胜|||REQUIRED|||-NONE-|||0
A 17 18|||S|||然后|||REQUIRED|||-NONE-|||0
A 21 21|||M|||了 你|||REQUIRED|||-NONE-|||0

S 我 绝对 不 能 忘记 和 你 在 一起 的 时间 。 我 真 感谢 你 。
A 3 4|||S|||会|||REQUIRED|||-NONE-|||0
A 14 14|||M|||的 很|||REQUIRED|||-NONE-|||0
A 3 4|||S|||会|||REQUIRED|||-NONE-|||1
A 13 14|||S|||很|||REQUIRED|||-NONE-|||1

S 八月 末 ， 你 和 你 的 男朋友 邀请 你 的 婚礼 。
A 9 9|||M|||我 参加|||REQUIRED|||-NONE-|||0

S 我 跟 老师 没 联系 已经 6 年 多 了 。
A 3 9|||W|||已经 6 年 多 没 联系|||REQUIRED|||-NONE-|||0

S 这 是 我 第一 次 给 你 写 的 信 ， 所以 我 不 知道 怎么 开始 。
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 国庆 期间 ， 我 多 看 北京 的 名所 ， 多 吃 北京 的 名菜 。
A 4 6|||S|||看 了 很多|||REQUIRED|||-NONE-|||0
A 8 9|||S|||名胜古迹|||REQUIRED|||-NONE-|||0
A 10 12|||S|||吃 了 很多|||REQUIRED|||-NONE-|||0
A 4 6|||S|||看 了 很多|||REQUIRED|||-NONE-|||1
A 8 9|||S|||名胜古迹|||REQUIRED|||-NONE-|||1
A 10 12|||S|||吃 了 很多|||REQUIRED|||-NONE-|||1

S 你 还 记得 我们 第一 次 见面 ？
A 7 7|||M|||的 时候 吗|||REQUIRED|||-NONE-|||0

S 但是 现在 我 的 汉语 水平 进步 了 跟 你 聊天 没 问题 了 。
A 6 6|||M|||已经|||REQUIRED|||-NONE-|||0
A 7 7|||M|||到|||REQUIRED|||-NONE-|||0
A 13 13|||M|||的 程度|||REQUIRED|||-NONE-|||0
A 8 8|||M|||，|||REQUIRED|||-NONE-|||1
A 11 11|||M|||已经|||REQUIRED|||-NONE-|||1

S 我 现在 跟 以前 一样 不 那么 忧郁 ， 反而 现在 性格 变 开朗 得 不得了 。
A 3 8|||S|||总是 忧郁 的 以前 不 一样 了|||REQUIRED|||-NONE-|||0
A 12 13|||S|||变得|||REQUIRED|||-NONE-|||0

S 最近 我 开始 第一 学期 ， 很 努力 学习 。
A 3 3|||M|||了|||REQUIRED|||-NONE-|||0
A 6 6|||M|||在|||REQUIRED|||-NONE-|||0
A 8 8|||M|||地|||REQUIRED|||-NONE-|||0
A 3 3|||M|||了|||REQUIRED|||-NONE-|||1
A 4 4|||M|||个|||REQUIRED|||-NONE-|||1
A 6 9|||W|||学习 很 努力|||REQUIRED|||-NONE-|||1

S 这里 的 学习 比 青岛 更 紧张 ， 但是 我 能 感觉 到 我 的 汉语 逐渐 地 提高 。
A 16 16|||M|||在|||REQUIRED|||-NONE-|||0

S 有时候 ， 备课 和 讲课 让 我 累 。
A 7 7|||M|||感到|||REQUIRED|||-NONE-|||0
A 7 8|||S|||疲惫|||REQUIRED|||-NONE-|||0
A 7 8|||S|||疲惫|||REQUIRED|||-NONE-|||1

S 但是 一 个 月 之后 我 再 见 过 你 的 时候 ， 我 看到 你 进步 的 样子 。
A 8 9|||S|||到|||REQUIRED|||-NONE-|||0
A 15 15|||M|||了|||REQUIRED|||-NONE-|||0

S 半 年 后 ， 我们 完全 只 用 韩语 能 交流 ， 真 让 我 大吃一惊 。
A 5 5|||M|||能|||REQUIRED|||-NONE-|||0
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 你 告诉 我 只要 不 放弃 ， 就 我们 的 人生 中 没有 失败 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 12 12|||M|||就|||REQUIRED|||-NONE-|||0
A 7 12|||W|||我们 的 人生 中 就|||REQUIRED|||-NONE-|||1

S 我 在 北大 学习 的 第一 年 我 吃 了 不少 苦 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 我 每 天 要 预习 和 复习 ， 努力 完成 老师 的 任务 。
A 3 3|||M|||都|||REQUIRED|||-NONE-|||0

S 我 和 家人 都 一起 在 关岛 过 了 很 幸福 的 时间 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 7 8|||S|||度过|||REQUIRED|||-NONE-|||0
A 9 9|||M|||一 段|||REQUIRED|||-NONE-|||0
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 7 8|||S|||度过|||REQUIRED|||-NONE-|||1
A 9 9|||M|||一 段|||REQUIRED|||-NONE-|||1
A 12 13|||S|||时光|||REQUIRED|||-NONE-|||1

S 从 那天 开始 ， 我 一直 在 作 作业 忙 着 。
A 7 7|||M|||忙 着|||REQUIRED|||-NONE-|||0
A 7 8|||S|||做|||REQUIRED|||-NONE-|||0
A 9 11|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 最近 天气 越来越 冷 了 ， 你 注意 感冒 ！
A 8 8|||M|||别|||REQUIRED|||-NONE-|||0

S 不 在 我 的 丹东 像 没 鱼 的 鸭绿江 一样 吧 ？
A 0 3|||W|||我 不 在|||REQUIRED|||-NONE-|||0

S 老师 讲课 时 ， 那 既 独特 ， 又 美妙 的 声音 我 忘不了 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 13 13|||M|||一直|||REQUIRED|||-NONE-|||0

S 我 起床 的 很 难 ， 总是 再 睡觉 。
A 2 3|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 4 5|||S|||困难|||REQUIRED|||-NONE-|||0
A 7 8|||S|||想|||REQUIRED|||-NONE-|||0

S 我 以前 上 了 高中 的 时候 ， 我 一 句 话 也 说 不 出来 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 你 选 了 我 ， 心理 很 受 感动 了 。
A 5 6|||S|||使 我|||REQUIRED|||-NONE-|||0
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 5 6|||S|||我 心里|||REQUIRED|||-NONE-|||1
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 我 被 交通 事故 了 。
A 1 2|||S|||遇到|||REQUIRED|||-NONE-|||0

S 我 听说 最近 韩国 来 台风 的 消息 ， 你 家 哪儿 一切 好 吧 ？
A 1 3|||W|||最近 听说|||REQUIRED|||-NONE-|||0
A 11 12|||S|||那儿|||REQUIRED|||-NONE-|||0
A 13 13|||M|||还|||REQUIRED|||-NONE-|||0

S 现在 几乎 每 天 的 空气 污染 指数 很 高 ， 天上 蒙 着 厚厚 的 烟雾 ， 再 加上 偶耳 刮 大风 的 时候 随 之 一 起 刮 的 沙尘 ， 对 身体 的 压力 还是 很 大 啊 。
A 16 17|||S|||雾霾|||REQUIRED|||-NONE-|||0
A 20 21|||S|||偶尔|||REQUIRED|||-NONE-|||0
A 29 30|||S|||刮起|||REQUIRED|||-NONE-|||0
A 8 8|||M|||都|||REQUIRED|||-NONE-|||1
A 16 17|||S|||雾霾|||REQUIRED|||-NONE-|||1
A 20 21|||S|||偶尔|||REQUIRED|||-NONE-|||1
A 29 30|||S|||刮起|||REQUIRED|||-NONE-|||1
A 36 37|||S|||危害|||REQUIRED|||-NONE-|||1

S 或许 你们 看 过 在 奥运会 上 默默 地 跑马拉松 的 选手 吗 ？
A 0 1|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 光明 不 能 拿 钱 买 的 ， 不 能 容易 求取 的 。
A 1 1|||M|||是|||REQUIRED|||-NONE-|||0
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 11 12|||S|||获取|||REQUIRED|||-NONE-|||0
A 1 1|||M|||是|||REQUIRED|||-NONE-|||1
A 9 10|||S|||轻易|||REQUIRED|||-NONE-|||1
A 11 12|||S|||获取|||REQUIRED|||-NONE-|||1

S 大多数 人 ， 一旦 在 黑暗 下 的 不 知道 自己 怎么 面对 这些 困难 。
A 7 8|||S|||就|||REQUIRED|||-NONE-|||0

S 其次 需要 对 黑暗 的 思考 方法 。
A 2 2|||M|||思考|||REQUIRED|||-NONE-|||0
A 2 3|||S|||面对|||REQUIRED|||-NONE-|||0
A 5 6|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 有 经历 摆脱 黑暗 的 人 ， 可以 面对 更 难 、 更 深 的 黑暗 ， 在 这 个 时候 才 能 得到 自信感 和 勇气 。
A 1 4|||W|||摆脱 黑暗 经历|||REQUIRED|||-NONE-|||0
A 24 25|||S|||自信|||REQUIRED|||-NONE-|||0

S 慢慢 的 走 黑暗 中 走 出来 ， 需要 的 是 决心 和 桓心 ， 还 需要 通气 。
A 1 2|||S|||地|||REQUIRED|||-NONE-|||0
A 2 3|||S|||从|||REQUIRED|||-NONE-|||0
A 13 14|||S|||恒心|||REQUIRED|||-NONE-|||0
A 17 18|||S|||勇气|||REQUIRED|||-NONE-|||0

S 是 在 新 石器 时代 ， 人类 开始 害怕 黑暗 ， 所以 需要 光明 为了 避免 恐惧 的 心理 。
A 1 1|||M|||因为|||REQUIRED|||-NONE-|||0
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 12 19|||M|||为了 避免 恐惧 的 心理 而 需要 光明|||REQUIRED|||-NONE-|||0

S 光明 与 许是贤 王 的 统治 ， 也许 是 无 战争 的 和平 。
A 1 2|||S|||也|||REQUIRED|||-NONE-|||0

S 代表 黑暗 的 形态 也 很多 : 疾病 、 天灾 、 战争 ， 竞争 激烈 的 现代 社会 带来 的 压力 ， 环境 污染 等 。
A 3 4|||S|||概念|||REQUIRED|||-NONE-|||0
A 5 5|||M|||有|||REQUIRED|||-NONE-|||0

S 既 使 它们 都 存在 过 ， 但 很 少 一 种 状况 持续 很 长 时间 。
A 0 1|||S|||即|||REQUIRED|||-NONE-|||0
A 10 10|||M|||会 有|||REQUIRED|||-NONE-|||0

S 有 疾病 就 有 药疗 ， 有 死亡 就 有 生命 。
A 4 5|||S|||医疗|||REQUIRED|||-NONE-|||0

S 一 个 小孩 在 阳光明 眉 的 大白天 感到 平安 和 舒服感 ， 但 一旦 放到 漆黑 的 房间 里 或 在 晚上 让 他 自己 呆 在 外面 ， 孩子 就 开始 哇哇 大哭 。
A 4 6|||S|||阳光明媚|||REQUIRED|||-NONE-|||0
A 7 8|||S|||白天 会|||REQUIRED|||-NONE-|||0
A 11 12|||S|||舒服|||REQUIRED|||-NONE-|||0
A 21 22|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 25 26|||S|||独自|||REQUIRED|||-NONE-|||0
A 32 32|||M|||会|||REQUIRED|||-NONE-|||0

S 她 说话 非常 有 幽默 讨人 喜欢 ， 所以 她 在 许多 节目 上当 了 主持人 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 5 5|||M|||又|||REQUIRED|||-NONE-|||0

S 他 每 天 过 非常 寒冷 ， 困苦 的 日子 。
A 4 4|||M|||着|||REQUIRED|||-NONE-|||0
A 6 7|||S|||、|||REQUIRED|||-NONE-|||0

S 有 一 天 ， 一 个 大学生 玩 着 滑板 ， 在 他 眼 前 过去 。
A 11 12|||S|||从|||REQUIRED|||-NONE-|||0
A 15 16|||S|||经过|||REQUIRED|||-NONE-|||0

S 他 请 他 的 朋友 借 钱 ， 不过 他们 都 拒绝 了 。
A 1 2|||S|||请求|||REQUIRED|||-NONE-|||0
A 7 7|||M|||给 他|||REQUIRED|||-NONE-|||0
A 1 2|||S|||向|||REQUIRED|||-NONE-|||1

S 看到 光明 与 黑暗 ， 我 会 不得不 想起 我 的 高三 时期 。
A 7 8|||S|||不由得|||REQUIRED|||-NONE-|||0
A 7 8|||S|||不由得|||REQUIRED|||-NONE-|||1

S 她 的 人生 就 在 那 黑暗 世界 里 ， 就 像 永远 找 不 到 光明 一样 的 时候 ， 导师 安妮 帮助 了 她 走出 黑暗 世界 。
A 1 2|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 2 3|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 10 11|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 11 12|||S|||仿佛|||REQUIRED|||-NONE-|||0
A 17 18|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 他 的 家庭 也 没 那么 富裕 ， 当 他 长大 以后 以 打工 在 车库 里 而 生活 。
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 12 13|||S|||靠|||REQUIRED|||-NONE-|||0
A 17 18|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 人生 就 是 过山车 ， 又 高 又 低 ， 又 光明 又 黑暗 。
A 5 6|||S|||有|||REQUIRED|||-NONE-|||0
A 7 8|||S|||有|||REQUIRED|||-NONE-|||0
A 10 11|||S|||有|||REQUIRED|||-NONE-|||0
A 13 13|||M|||有|||REQUIRED|||-NONE-|||0

S 回想 那 一些 人生 的 一 部分 ， 我 才 能 明白 人生 里 存在 的 光明 与 黑暗 是 必须 得 存在 的 要素 。
A 2 3|||S|||些|||REQUIRED|||-NONE-|||0
A 4 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 10 11|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 14 16|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 19 19|||M|||都|||REQUIRED|||-NONE-|||0
A 21 23|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 虽然 这 新 世界 我 不 知道 ， 不 熟悉 ， 但是 有 我 的 父母 ， 就 能 战胜 未来 。
A 7 8|||S|||、|||REQUIRED|||-NONE-|||0
A 17 17|||M|||我|||REQUIRED|||-NONE-|||0

S 仅 是 这 两 次 的 经验 ， 多数 次 的 光明 与 黑暗 影响 了 我 的 人生 。
A 0 1|||S|||不仅|||REQUIRED|||-NONE-|||0
A 8 9|||S|||无数|||REQUIRED|||-NONE-|||0
A 0 1|||S|||不仅|||REQUIRED|||-NONE-|||1
A 8 9|||S|||无数|||REQUIRED|||-NONE-|||1

S 我 低 幼 儿童 的 时候 很 想 养 狗 。
A 1 4|||S|||小|||REQUIRED|||-NONE-|||0

S 我 班 里 有 很 漂亮 的 女 同学 ， 我 一 见 钟情 。
A 4 4|||M|||位|||REQUIRED|||-NONE-|||0
A 11 11|||M|||对 她|||REQUIRED|||-NONE-|||0

S 所以 一 段 时期 ， 一 天 吃 一 吨 饭 ， 睡 得 不 好 。
A 1 1|||M|||有|||REQUIRED|||-NONE-|||0
A 7 7|||M|||只|||REQUIRED|||-NONE-|||0
A 9 10|||S|||顿|||REQUIRED|||-NONE-|||0
A 14 14|||M|||也|||REQUIRED|||-NONE-|||0

S 妈 也 第一 次 打 我 的 ， 我 也 第一 次 被 妈打 的 。
A 6 7|||S|||了|||REQUIRED|||-NONE-|||0
A 14 15|||S|||了|||REQUIRED|||-NONE-|||0

S 还 解放 后 发生 6·25 战争 ， 因此 这些 给 韩国 带来 的 打击 很 大 。
A 0 1|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 3 3|||M|||还|||REQUIRED|||-NONE-|||0
A 4 4|||M|||了|||REQUIRED|||-NONE-|||0
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 0 3|||W|||解放 后 还|||REQUIRED|||-NONE-|||1
A 4 4|||M|||了|||REQUIRED|||-NONE-|||1
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 现在 到 韩国 的 光明 时期 的 原因 是 ， 通过 黑暗 的 时代 充满 着 如同 劲草 般 刚 强 不 屈 的 好胜心 。
A 1 2|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 3 4|||S|||达到 了|||REQUIRED|||-NONE-|||0
A 10 11|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 1 2|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 3 4|||S|||达到 了|||REQUIRED|||-NONE-|||1
A 14 16|||S|||磨练 了|||REQUIRED|||-NONE-|||1

S 在 成长 的 过程 中 不 摔跤 是 不 可能 的 ， 但 会 有 恐惧 ， 如果 再 过 这 关 会 得到 光明 。
A 12 13|||S|||虽然|||REQUIRED|||-NONE-|||0
A 17 17|||M|||但|||REQUIRED|||-NONE-|||0
A 18 19|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 19 20|||S|||度过|||REQUIRED|||-NONE-|||0
A 22 22|||M|||就|||REQUIRED|||-NONE-|||0

S 最近 很多 有 被 偷 东西 的 事情 。 所以 以免 发生 同样 的 事情 ， 请 学生们 注意 。
A 1 3|||W|||有 很多|||REQUIRED|||-NONE-|||0
A 8 9|||S|||，|||REQUIRED|||-NONE-|||0
A 10 11|||S|||为 避免|||REQUIRED|||-NONE-|||0
A 1 3|||W|||有 很多|||REQUIRED|||-NONE-|||1
A 3 6|||W|||东西 被 偷|||REQUIRED|||-NONE-|||1
A 8 9|||S|||，|||REQUIRED|||-NONE-|||1
A 10 11|||S|||为 避免|||REQUIRED|||-NONE-|||1

S 这 个 宿舍 的 隔声 不 好 ， 在 晚上 时 ， 别 说 大声 。
A 13 14|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 15 15|||M|||说话|||REQUIRED|||-NONE-|||0

S 请 勿 晚上 9点 以后 发生 大 声音 。
A 2 2|||M|||在|||REQUIRED|||-NONE-|||0
A 5 6|||S|||发出|||REQUIRED|||-NONE-|||0
A 7 8|||S|||声响|||REQUIRED|||-NONE-|||0

S 请 勿 培养 宠物 。
A 2 3|||S|||养|||REQUIRED|||-NONE-|||0

S 严禁 宿舍楼 内 抽烟 。
A 1 1|||M|||在|||REQUIRED|||-NONE-|||0

S 请 勿 在 楼道 吵嚷 。
A 4 5|||S|||吵闹|||REQUIRED|||-NONE-|||0

S 有 麻烦 的 事 ， 走来 管理处 。
A 0 1|||S|||遇到|||REQUIRED|||-NONE-|||0
A 5 6|||S|||请来|||REQUIRED|||-NONE-|||0

S 为了 更 好 的 住宿 环境 ， 请 学生们 准守 以下 几 个 内容 。
A 1 1|||M|||营造|||REQUIRED|||-NONE-|||0
A 9 10|||S|||遵守|||REQUIRED|||-NONE-|||0
A 13 14|||S|||要求|||REQUIRED|||-NONE-|||0

S 韩国 战争 结束 后 的 韩国 是 一 片 黑暗 的 国家 ， 什么 地方 也 没有 吃 的 、 穿 的 。
A 0 1|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 7 7|||M|||个|||REQUIRED|||-NONE-|||0
A 15 16|||S|||都|||REQUIRED|||-NONE-|||0

S 但 过 了 许多 年 韩国 得到 世界 贸易 的 中心 。
A 5 5|||M|||后|||REQUIRED|||-NONE-|||0
A 6 7|||S|||成为 了|||REQUIRED|||-NONE-|||0

S 考试 不 极 各 了 怎么 办 ？
A 2 4|||S|||及格|||REQUIRED|||-NONE-|||0

S 面临 困境 的 时候 我们 常常 很 容易 地 抛弃 克服 那些 困境 的 机会 ， 那 可能 因为 是 我们 没有 勇气 ， 人生 中 没有 遇到 过 那么 大 的 困境 。
A 9 10|||S|||放弃|||REQUIRED|||-NONE-|||0
A 9 10|||S|||放弃|||REQUIRED|||-NONE-|||1
A 18 20|||W|||是 因为|||REQUIRED|||-NONE-|||1

S 这 段 话 给 我们 有 道理 的 ， 虽然 现在 黑暗 ， 但 等 着 、 忍 着 ， 光明 会 到来 的 。
A 3 5|||S|||是|||REQUIRED|||-NONE-|||0
A 11 11|||M|||是|||REQUIRED|||-NONE-|||0
A 3 5|||S|||是|||REQUIRED|||-NONE-|||1
A 21 21|||M|||就|||REQUIRED|||-NONE-|||1
A 23 24|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 无论 现在 怎么样 ， 我们 努力 下去 ， 光明 不 到 远 的 。
A 4 4|||M|||只要|||REQUIRED|||-NONE-|||0
A 9 9|||M|||就|||REQUIRED|||-NONE-|||0
A 10 11|||S|||会|||REQUIRED|||-NONE-|||0
A 11 12|||S|||遥远|||REQUIRED|||-NONE-|||0
A 12 13|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 一般 人 想 光明 是 好 的 ， 就 把 好事 比喻 在 光明 ， 就 有 了 “ 光明 磊落 ” 这 种 成语 。
A 2 3|||S|||认为|||REQUIRED|||-NONE-|||0
A 11 13|||S|||比作|||REQUIRED|||-NONE-|||0

S 虽然 这 种 想法 是 人类 产生 之后 一直 保存 下来 的 ， 但是 我 想 如果 没有 黑暗 的话 ， 我们 就 看 不 到 光明 的 辉光 。
A 4 5|||S|||从|||REQUIRED|||-NONE-|||0
A 6 7|||S|||诞生|||REQUIRED|||-NONE-|||0
A 8 8|||M|||就|||REQUIRED|||-NONE-|||0
A 9 10|||S|||存在|||REQUIRED|||-NONE-|||0
A 6 7|||S|||诞生|||REQUIRED|||-NONE-|||1
A 28 29|||S|||光辉|||REQUIRED|||-NONE-|||1

S 因为 我们 在 夜里 休息 ， 早上 可以 更 用力 ， 专心 自己 要 做 的 工作 。
A 6 6|||M|||所以|||REQUIRED|||-NONE-|||0
A 9 10|||S|||努力|||REQUIRED|||-NONE-|||0
A 10 11|||S|||、|||REQUIRED|||-NONE-|||0
A 12 12|||M|||地 做|||REQUIRED|||-NONE-|||0
A 6 6|||M|||所以|||REQUIRED|||-NONE-|||1
A 9 10|||S|||努力|||REQUIRED|||-NONE-|||1
A 12 12|||M|||于|||REQUIRED|||-NONE-|||1

S 世界 历史 上 成就 到 的 伟大 的 业绩 都 是 追求 光明 而 得以 实现 的 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 11 11|||M|||通过|||REQUIRED|||-NONE-|||0
A 13 14|||S|||来|||REQUIRED|||-NONE-|||0
A 14 15|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 11 11|||M|||通过|||REQUIRED|||-NONE-|||1

S 对 现代 科学 发展 引起 了 巨大 的 影响 。
A 4 5|||S|||产生|||REQUIRED|||-NONE-|||0

S 他 开始 找 如此 感到 黑喑 的 理由 ， 但 找 不 着 理由 了 。
A 3 5|||W|||感到 如此|||REQUIRED|||-NONE-|||0
A 12 15|||S|||到|||REQUIRED|||-NONE-|||0

S 从此 之后 ， 他 退休 了 公司 ， 就 开始 在 社会 服务 团体 里 参加 活动 。
A 4 7|||W|||从 公司 退休 了|||REQUIRED|||-NONE-|||0
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 虽然 这 两 则 的 性格 很 相同 ， 但是 这 两 则 不 能 同时 存在 谋 一 件 事 上 。
A 3 4|||S|||者|||REQUIRED|||-NONE-|||0
A 5 6|||S|||类型|||REQUIRED|||-NONE-|||0
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 12 13|||S|||者|||REQUIRED|||-NONE-|||0
A 16 17|||S|||存在于|||REQUIRED|||-NONE-|||0
A 17 18|||S|||某|||REQUIRED|||-NONE-|||0
A 3 4|||S|||者|||REQUIRED|||-NONE-|||1
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 12 13|||S|||者|||REQUIRED|||-NONE-|||1
A 17 17|||M|||于|||REQUIRED|||-NONE-|||1
A 17 18|||S|||某|||REQUIRED|||-NONE-|||1

S 互联网 成 了 对 我们 的 生活 很 重要 的 陪分 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 7 7|||M|||中|||REQUIRED|||-NONE-|||0
A 10 11|||S|||部分|||REQUIRED|||-NONE-|||0

S 但是 ， 上网 也 有 些 不 好 的 点 。
A 9 10|||S|||地方|||REQUIRED|||-NONE-|||0

S 我们 迎来 了 最 和 美 的 中秋 佳节 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 途中 又 很 不幸 恰逢 班机 延误 。
A 4 5|||S|||遇到|||REQUIRED|||-NONE-|||0

S 但 充满 了 每 个 学生 的 心意 和 温暖 。
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 9 10|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 此 次 活动 的 主要 目的 是 要 拉进 师生 之间 的 关系 。
A 7 8|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 听 他 母亲 说 他 是 副经历 。
A 6 7|||S|||副经理|||REQUIRED|||-NONE-|||0

S 本 公司 为 远方 地区 的 难民 捐出 一 大 笔 款 。
A 2 2|||M|||曾|||REQUIRED|||-NONE-|||0
A 11 12|||S|||捐款|||REQUIRED|||-NONE-|||0
A 11 12|||S|||钱|||REQUIRED|||-NONE-|||1

S 我 希望 将来 会 像 贵 公司 为 别人 做出 贡献 。
A 2 2|||M|||我|||REQUIRED|||-NONE-|||0
A 7 7|||M|||一样|||REQUIRED|||-NONE-|||0

S 手机 是 三星 品牌 的 、 后面 贴 着 许多 小型 动物 的 贴子 。
A 10 11|||S|||小|||REQUIRED|||-NONE-|||0
A 13 14|||S|||贴纸|||REQUIRED|||-NONE-|||0

S 直 到 二十 世纪 末 ， 印尼 政府 《 精确 拼音 》 的 发布 才 有所 改变 。
A 6 6|||M|||随着|||REQUIRED|||-NONE-|||0

S 如 在 美国 与 英国 ， 尽管 在 拼写 上 的 差别 很 小 。
A 7 7|||M|||各自 的 英语|||REQUIRED|||-NONE-|||0

S 英语 是 一 种 不折不扣 的 语言 。
A 1 1|||M|||不折不扣|||REQUIRED|||-NONE-|||0
A 4 5|||S|||普遍 使用|||REQUIRED|||-NONE-|||0

S 大多数 程序 是 用 英语 偏 写 的 。
A 5 7|||S|||编写|||REQUIRED|||-NONE-|||0

S 培育 了 中华 民族 成长 的 汉语 又 称 中文 、 华语 、 国语 、 中 国语 等 其他 名称 。
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 8 9|||S|||有|||REQUIRED|||-NONE-|||0
A 4 5|||R|||-NONE-|||REQUIRED|||-NONE-|||1
A 18 20|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 可 变得 有趣 多 了 。
A 1 2|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 其中 就 是 对 数字 能力 的 影响 。
A 2 3|||S|||有|||REQUIRED|||-NONE-|||0

S 与 一些 亚洲 国家 产生 较大 距离 。
A 4 5|||S|||有|||REQUIRED|||-NONE-|||0
A 5 5|||M|||了|||REQUIRED|||-NONE-|||1
A 6 6|||M|||的|||REQUIRED|||-NONE-|||1

S 中 美 两 国 的 数学 能力 存在 着 差异 。
A 9 9|||M|||明显|||REQUIRED|||-NONE-|||0

S 从 第四 类 的 混种语 可 见 日本人 比 交 喜欢 四 个 假名 的 字 。
A 8 10|||S|||比较|||REQUIRED|||-NONE-|||0
A 8 10|||S|||比较|||REQUIRED|||-NONE-|||1

S 它 吸收 的 各 个 国家 的 文化 。 非常 复杂 ， 所以 学 起来 比较 难 。
A 2 3|||S|||了|||REQUIRED|||-NONE-|||0
A 8 9|||S|||，|||REQUIRED|||-NONE-|||0

S 但是 因为 ， 这么 模糊 又 复杂 ， 所以 学起来 很 有趣 。
A 2 3|||S|||它|||REQUIRED|||-NONE-|||0

S 这时 表达 心 在 七上八下 。
A 0 1|||S|||这 是|||REQUIRED|||-NONE-|||0
A 0 1|||S|||这 是|||REQUIRED|||-NONE-|||1
A 2 3|||S|||心情|||REQUIRED|||-NONE-|||1
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 大多数 程序 是 用 英语 偏 写 的 。
A 5 7|||S|||编写|||REQUIRED|||-NONE-|||0

S 不慎 将 手机 遗失 在 草平 里 。
A 5 6|||S|||草坪|||REQUIRED|||-NONE-|||0

S 实际上 两 者 是 存在 着 差异 的 。 就 好比 英式 英语 和 美式 英语 。
A 8 9|||S|||，|||REQUIRED|||-NONE-|||0

S 对于 打发 周末 时间 没有 具体 的 安排 。
A 1 1|||M|||如何|||REQUIRED|||-NONE-|||0

S 怎么 过 时间 吗 ？
A 1 2|||S|||打发|||REQUIRED|||-NONE-|||0
A 3 4|||S|||呢|||REQUIRED|||-NONE-|||0

S 留学 的 城市 就 是 北京 、 大连 、 杭州 和 徐州 总4 个 地方 。
A 3 4|||R|||-NONE-|||REQUIRED|||-NONE-|||0
A 12 13|||S|||总共 4|||REQUIRED|||-NONE-|||0

S 晚上 还 要 做 作业 或者 跟 朋友们 一起 完儿睡晚觉 。
A 9 10|||S|||玩 到 很 晚 才 睡觉|||REQUIRED|||-NONE-|||0

S 就 是 为了 补充 精神 、 练习 汉语 。
A 3 4|||S|||养足|||REQUIRED|||-NONE-|||0

S 在 留学 生活 中 需要 很多 钱 ， 有 人 难得 开支 另外 的 费用 。
A 10 11|||S|||很 难 担负|||REQUIRED|||-NONE-|||0

S 她 在 周末 不 学习 就 不 能 追赶 汉语 课 因为 她 觉得 课 很 难 。
A 11 11|||M|||的 进度，|||REQUIRED|||-NONE-|||0

S 留学 的 主要 目的 就 是 对 外国 的 文化 习惯 和 学习 那 个 国家 的 文化 。
A 6 7|||S|||学习|||REQUIRED|||-NONE-|||0
A 12 13|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 让 留学生 的 留学 经验 更 有 获得 。
A 2 8|||W|||获得 更 多 留学 经验|||REQUIRED|||-NONE-|||0

S 只 性格 和 体力 这 调查 有点 瓜葛 。
A 0 1|||S|||只有|||REQUIRED|||-NONE-|||0
A 5 5|||M|||个|||REQUIRED|||-NONE-|||0
A 6 6|||M|||结果|||REQUIRED|||-NONE-|||0
A 7 8|||S|||关系|||REQUIRED|||-NONE-|||0

S 他们 为了 健康 投资 了 自己 的 身体 上 。
A 8 9|||R|||-NONE-|||REQUIRED|||-NONE-|||0

S 就 学习 英语 或者 专业 方面 的 。
A 7 7|||M|||知识|||REQUIRED|||-NONE-|||0

S 最少 比例 是 去 宗教 活动 和 跟 朋友们 见面 。
A 2 2|||M|||的 活动|||REQUIRED|||-NONE-|||0
A 6 7|||S|||或者|||REQUIRED|||-NONE-|||0

S 我们 有 七 个 音乐队 72 天 的 练习 。
A 5 5|||M|||进行 了 总共|||REQUIRED|||-NONE-|||0

S 您 给予 的 对 我 来说 是 一 个 很 好 的 机会 。
A 2 7|||S|||了 我|||REQUIRED|||-NONE-|||0

S 因此 ， 上周 应该 要 交 的 文件 。
A 3 4|||S|||本 应该|||REQUIRED|||-NONE-|||0
A 3 4|||S|||本 应该|||REQUIRED|||-NONE-|||1
A 6 7|||R|||-NONE-|||REQUIRED|||-NONE-|||1

S 可以 非常 容易 交流 自己 系 的 同学 。
A 3 4|||S|||和|||REQUIRED|||-NONE-|||0
A 8 8|||M|||交流|||REQUIRED|||-NONE-|||0
A 3 3|||M|||地 和|||REQUIRED|||-NONE-|||1
A 3 8|||W|||自己 系 的 同学 交流|||REQUIRED|||-NONE-|||1

S 但是 难于 与 别 的 系 的 同学 或 老师们 。
A 10 10|||M|||交流|||REQUIRED|||-NONE-|||0
A 1 2|||S|||很 难|||REQUIRED|||-NONE-|||1
A 10 10|||M|||交流|||REQUIRED|||-NONE-|||1

S 这 问卷 调查 的 目的 询问 北大 留学生 的 周末 生活 。
A 5 5|||M|||是|||REQUIRED|||-NONE-|||0
A 5 5|||M|||是|||REQUIRED|||-NONE-|||1
A 11 11|||M|||情况|||REQUIRED|||-NONE-|||1

S 因此 我 不 能 担任 进行 辅助 人员 。
A 5 6|||S|||活动 的|||REQUIRED|||-NONE-|||0
A 5 6|||S|||做|||REQUIRED|||-NONE-|||1
A 8 8|||M|||的 工作|||REQUIRED|||-NONE-|||1

S 老师们 找出 汉语 优秀 的 学生 。
A 1 1|||M|||能|||REQUIRED|||-NONE-|||0

S 造成 了 电脑 爆发 的 事故 。
A 3 4|||S|||爆炸|||REQUIRED|||-NONE-|||0

