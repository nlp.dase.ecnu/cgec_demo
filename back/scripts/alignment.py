import jieba.posseg as pseg
from rapidfuzz.distance import Indel
from itertools import combinations, groupby
from edit import Edit
from gramMatch import word_matching, word_patterns, word_class_map1, word_class_map2, class_class_map, find_label_father

def align(orig_cut, cor_cut, orig_pos, cor_pos, orig_gram, cor_gram, lev):

    o_len = len(orig_cut)
    c_len = len(cor_cut)
    # Lower case token IDs (for transpositions)
    # o_low = [o.lower for o in orig_cut]
    # c_low = [c.lower for c in cor_cut]
    # Create the cost_matrix and the op_matrix
    cost_matrix = [[0.0 for j in range(c_len + 1)] for i in range(o_len + 1)]
    op_matrix = [["O" for j in range(c_len + 1)] for i in range(o_len + 1)]
    # Fill in the edges
    for i in range(1, o_len + 1):
        cost_matrix[i][0] = cost_matrix[i - 1][0] + 1
        op_matrix[i][0] = "D"
    for j in range(1, c_len + 1):
        cost_matrix[0][j] = cost_matrix[0][j - 1] + 1
        op_matrix[0][j] = "I"

    for i in range(o_len):
        for j in range(c_len):
            # Matches
            if orig_cut[i] == cor_cut[j]:
                cost_matrix[i + 1][j + 1] = cost_matrix[i][j]
                op_matrix[i + 1][j + 1] = "M"
            # Non-matches
            else:
                del_cost = cost_matrix[i][j + 1] + 1
                ins_cost = cost_matrix[i + 1][j] + 1
                trans_cost = float("inf")
                # Standard Levenshtein (S = 1)
                if lev:
                    sub_cost = cost_matrix[i][j] + 1
                # Linguistic Damerau-Levenshtein
                else:
                    # Custom substitution
                    sub_cost = cost_matrix[i][j] + \
                               get_sub_cost(orig_cut[i], cor_cut[j], orig_pos[i], cor_pos[j], orig_gram[i], cor_gram[j])
                    # Transpositions require >=2 tokens
                    # Traverse the diagonal while there is not a Match.
                    k = 1
                    while i - k >= 0 and j - k >= 0 and \
                            cost_matrix[i - k + 1][j - k + 1] != cost_matrix[i - k][j - k]:
                        if sorted(orig_cut[i - k:i + 1]) == sorted(cor_cut[j - k:j + 1]):
                            trans_cost = cost_matrix[i - k][j - k] + k
                            break
                        k += 1
                # Costs
                costs = [trans_cost, sub_cost, ins_cost, del_cost]
                # Get the index of the cheapest (first cheapest if tied)
                l = costs.index(min(costs))
                # Save the cost and the op in the matrices
                cost_matrix[i + 1][j + 1] = costs[l]
                if l == 0:
                    op_matrix[i + 1][j + 1] = "T" + str(k + 1)
                elif l == 1:
                    op_matrix[i + 1][j + 1] = "S"
                elif l == 2:
                    op_matrix[i + 1][j + 1] = "I"
                else:
                    op_matrix[i + 1][j + 1] = "D"
    # Return the matrices
    return cost_matrix, op_matrix

def get_sub_cost(o, c, o_pos, c_pos, o_gram, c_gram):
    # Short circuit if the only difference is case
    open_pos = ["n", "f", "s", "nr", "ns", "nt", "nw", "nz", "v", "vd", "vn", "a", "ad", "an", "d"]
    if o.lower == c.lower: return 0
    # POS cost(0-0.49)
    if o_pos == c_pos: pos_cost = 0
    elif o_pos in open_pos and c_pos in open_pos: pos_cost = 0.5
    else: pos_cost = 0.49
    # Char cost (0-0.5)
    char_cost = Indel.normalized_distance(o, c)*0.5
    # Gram cost (0-1)
    o_gram_father = find_label_father(o_gram, word_class_map2)
    c_gram_father = find_label_father(c_gram, word_class_map2)
    o_gram_grandfather = class_class_map[o_gram_father]
    c_gram_grandfather = class_class_map[c_gram_father]
    if o_gram == "0" or c_gram == "0": gram_cost = 1
    elif o_gram == c_gram: gram_cost = 0
    elif o_gram_father == c_gram_father: gram_cost = 0.25
    elif o_gram_grandfather == c_gram_grandfather: gram_cost = 0.5
    else: gram_cost = 1
    # Combine the costs(0-1.99)
    return pos_cost + char_cost + gram_cost

def get_cheapest_align_seq(op_matrix):
    i = len(op_matrix)-1
    j = len(op_matrix[0])-1
    align_seq = []
    # Work backwards from bottom right until we hit top left
    while i + j != 0:
        # Get the edit operation in the current cell
        op = op_matrix[i][j]
        # Matches and substitutions
        if op in {"M", "S"}:
            align_seq.append((op, i-1, i, j-1, j))
            i -= 1
            j -= 1
        # Deletions
        elif op == "D":
            align_seq.append((op, i-1, i, j, j))
            i -= 1
        # Insertions
        elif op == "I":
            align_seq.append((op, i, i, j-1, j))
            j -= 1
        # Transpositions
        else:
            # Get the size of the transposition
            k = int(op[1:])
            align_seq.append((op, i-k, i, j-k, j))
            i -= k
            j -= k
    # Reverse the list to go from left to right and return
    align_seq.reverse()
    return align_seq

def get_rule_edits(align_seq, orig_cut, cor_cut, orig_pos, cor_pos):
    edits = []
    # Split alignment into groups of M, T and rest. (T has a number after it)
    options_map = {"T":"W", "I":"M", "D":"R", "S":"S"}
    for op, group in groupby(align_seq,
            lambda x: x[0][0]):  #Retain all options
        group = list(group)
        # print(op)
        # print(group)
        # Ignore M
        if op == "M": continue
        # T is always split
        elif op == "T":
            for seq in group:
                edits.append(Edit(orig_cut, cor_cut, seq[1:], options_map[op]))
        # Process D, I and S subsequence
        else:
            processed = process_seq(group, orig_cut, cor_cut, orig_pos, cor_pos)
            # Turn the processed sequence into edits
            for seq in processed:
                edits.append(Edit(orig_cut, cor_cut, seq[1:], options_map[op]))
    return edits

def process_seq(seq, orig_cut, cor_cut, orig_pos, cor_pos):
    # Return single alignments
    if len(seq) <= 1: return seq
    # Get the ops for the whole sequence
    ops = [op[0] for op in seq]
    # Merge all D xor I ops. (95% of human multi-token edits contain S).
    if set(ops) == {"D"} or set(ops) == {"I"}: return merge_edits(seq)

    content = False # True if edit includes a content word
    # Get indices of all start-end combinations in the seq: 012 = 01, 02, 12
    combos = list(combinations(range(0, len(seq)), 2))
    # Sort them starting with largest spans first
    combos.sort(key = lambda x: x[1]-x[0], reverse=True)
    # Loop through combos
    for start, end in combos:
        # Ignore ranges that do NOT contain a substitution.
        if "S" not in ops[start:end+1]: continue
        # Get the tokens in orig and cor. They will now never be empty.
        o = orig_cut[seq[start][1]:seq[end][2]]
        c = cor_cut[seq[start][3]:seq[end][4]]

        # Merge same POS or auxiliary/infinitive/phrasal verbs:
        # [to eat -> eating], [watch -> look at]
        # pos_set = set([tok.pos for tok in o]+[tok.pos for tok in c])
        # if len(o) != len(c) and (len(pos_set) == 1 or \
        #         pos_set.issubset({POS.AUX, POS.PART, POS.VERB})):
        #     return process_seq(seq[:start], alignment) + \
        #         merge_edits(seq[start:end+1]) + \
        #         process_seq(seq[end+1:], alignment)
        # Split rules take effect when we get to smallest chunks
        if end-start < 2:
            # Split adjacent substitutions
            if len(o) == len(c) == 2:
                return process_seq(seq[:start+1], orig_cut, cor_cut, orig_pos, cor_pos) + \
                    process_seq(seq[start+1:], orig_cut, cor_cut, orig_pos, cor_pos)
            # Split similar substitutions at sequence boundaries
            if (ops[start] == "S" and char_cost(o[0], c[0]) > 0.75) or \
                    (ops[end] == "S" and char_cost(o[-1], c[-1]) > 0.75):
                return process_seq(seq[:start+1], orig_cut, cor_cut, orig_pos, cor_pos) + \
                    process_seq(seq[start+1:], orig_cut, cor_cut, orig_pos, cor_pos)
            # Split final determiners
            # if end == len(seq)-1 and ((ops[-1] in {"D", "S"} and \
            #         o[-1].pos == POS.DET) or (ops[-1] in {"I", "S"} and \
            #         c[-1].pos == POS.DET)):
            #     return process_seq(seq[:-1], orig_cut, cor_cut, orig_pos, cor_pos) + [seq[-1]]
        # Set content word flag
        # if not pos_set.isdisjoint(open_pos): content = True
    # Merge sequences that contain content words
    if content: return merge_edits(seq)
    else: return seq


# Check whether token is punctuation
def is_punct(token):
    return token.pos == POS.PUNCT or token.text in punctuation

# Calculate the cost of character alignment; i.e. char similarity
def char_cost(a, b):
    return 1 - Indel.normalized_distance(a, b)


# Merge the input alignment sequence to a single edit span
def merge_edits(seq):
    if seq:
        return [("X", seq[0][1], seq[-1][2], seq[0][3], seq[-1][4])]
    else:
        return seq