from util import paragraphs
from gramMatch import word_class_map2
import pandas as pd

def load_annotation(gold_file):
    source_sentences = []
    gold_edits = []
    fgold = open(gold_file,encoding="utf-8")
    puffer = fgold.read()
    fgold.close()
    # puffer = puffer.decode('utf8')
    for item in paragraphs(puffer.splitlines(True)):
        item = item.splitlines(False)
        sentence = [line[2:].strip() for line in item if line.startswith('S ')]
        assert sentence != []
        annotations = {}
        for line in item[1:]:
            if line.startswith('I ') or line.startswith('S '):
                continue
            assert line.startswith('A ')
            line = line[2:]
            fields = line.split('|||')
            start_offset = int(fields[0].split()[0])
            end_offset = int(fields[0].split()[1])
            etype = fields[1]
            if etype == 'noop':
                start_offset = -1
                end_offset = -1
            corrections =  [c.strip() if c != '-NONE-' else '' for c in fields[2].split('||')]
            grammar = fields[3]
            # NOTE: start and end are *token* offsets
            original = ' '.join(' '.join(sentence).split()[start_offset:end_offset])
            annotator = int(fields[5])
            if annotator not in list(annotations.keys()):
                annotations[annotator] = []
            annotations[annotator].append((start_offset, end_offset, original, corrections, grammar))
        tok_offset = 0
        for this_sentence in sentence:
            tok_offset += len(this_sentence.split())
            source_sentences.append(this_sentence)
            this_edits = {}
            for annotator, annotation in annotations.items():
                this_edits[annotator] = [edit for edit in annotation if edit[0] <= tok_offset and edit[1] <= tok_offset and edit[0] >= 0 and edit[1] >= 0]
            if len(this_edits) == 0:
                this_edits[0] = []
            gold_edits.append(this_edits)
    return (source_sentences, gold_edits)

if __name__ == "__main__":
    gold_file = "../NLPCC_file/CGECEA_gram.01"
    out_file = "../results/dataset/NLPCC_count.csv"
    source_sentences, gold_edits = load_annotation(gold_file)
    # print(gold_edits)
    label_count = {'其他':0}
    total_edit_count = 0
    for key in word_class_map2:
        label_count[key] = 0
    for sentence in gold_edits:
        for annotator in sentence:
            for edit in sentence[annotator]:
                label_count[edit[4]] += 1
                total_edit_count += 1
    # for key in label_count:
    #     label_count[key] /= total_edit_count
    # print(label_count)
    df = pd.DataFrame.from_dict(label_count,orient='index')
    # print(df)
    df.to_csv(out_file, encoding="utf_8_sig")


