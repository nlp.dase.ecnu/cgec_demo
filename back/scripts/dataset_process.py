if __name__ == "__main__":
    file_in = "../NaCGEC/nacgec.dev.ref.para"
    file_out = "../NaCGEC/nacgec_dev.txt"
    with open(file_in, "r", encoding="utf-8") as fi, open(file_out, "w", encoding="utf-8") as fo:
        for line in fi:
            line = line.strip()
            # print(line)
            content = line.split('\t')
            if len(content) == 2:
                fo.write(content[1] + '\t' + content[1] + '\n')
            else:
                for target in content[2:]:
                    fo.write(content[1] + '\t' + target + '\n')