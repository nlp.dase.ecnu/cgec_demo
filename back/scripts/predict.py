from generate_m2 import seq2m2, m22m2_w_correct, m22m2_multireference
import os
import jieba.posseg as pseg
from segmentByJieba import segment
from evaluate import *
from alignment import *
from gleu.compute_gleu import compute_gleu_score
# from PT_M2 import evaluate_pt_m2
import subprocess

error_map = {
    'M':'单词缺失',
    'S':'单词选择错误',
    'R':'单词冗余',
    'W':'词序错误'
}

def process(origin_text, correct_text):
    orig = pseg.cut(origin_text)
    cor = pseg.cut(correct_text)
    orig_cut = []
    orig_pos = []
    orig_gram = []
    cor_cut = []
    cor_pos = []
    cor_gram = []
    for word, flag in orig:
        orig_cut.append(word)
        orig_pos.append(flag)
        orig_gram.append(word_matching(word))
    for word, flag in cor:
        cor_cut.append(word)
        cor_pos.append(flag)
        cor_gram.append(word_matching(word))
    print(" ".join(orig_cut))
    print(" ".join(cor_cut))
    lev = False
    cost_matrix, op_matrix = align(orig_cut, cor_cut, orig_pos, cor_pos, orig_gram, cor_gram, lev)
    align_seq = get_cheapest_align_seq(op_matrix)
    # print(align_seq)
    edits = get_rule_edits(align_seq, orig_cut, cor_cut, orig_pos, cor_pos)
    m2edits = ""
    edit_position = []
    for edit in edits:
        m2edits += edit.to_m2(0) + "\n"
        edit_position.append(edit.get_params())
    return m2edits, edit_position, orig_cut, cor_cut

def gleu_process(text_file, gleu_file):
    with open(text_file, "r", encoding="utf-8") as f, open(gleu_file, "w", encoding="utf-8") as gf:
        gold = []
        for line in f.readlines():
            if line.strip() == "": break
            gold_list = [char for char in line.strip()]
            gold.append(" ".join(gold_list))
        gf.write("\n".join(gold))

def predict(orig, cor):
    m2, edit_position, orig_cut, cor_cut = process(orig, cor)
    edits = []
    i = 0
    for line, position in zip(m2.split('\n'), edit_position):
        print(line)
        print(position)
        if (not line.strip()): break
        edit = {}
        fields = line.split('|||')
        edit["orig_start"] = len("".join(orig_cut[:position["o_start"]]))
        edit["orig_end"] = len("".join(orig_cut[:position["o_end"]]))
        edit["etype"] = fields[1] + "（" + error_map[fields[1]] + "）"
        # etype.append(fields[1])
        # etype.append(error_map[fields[1]])
        # if etype == 'noop':
        #     start_offset = -1
        #     end_offset = -1
        # corrections = [c.strip() if c != '-NONE-' else '' for c in fields[2].split('||')]
        # edit["corrections"] = "".join(corrections)
        edit["cor_start"] = len("".join(cor_cut[:position["c_start"]]))
        edit["cor_end"] = len("".join(cor_cut[:position["c_end"]]))
        edit["grammar"] = fields[3]
        edits.append(edit)
        i += 1
    return (edits)

def evaluate(predict_file, correct_file):
    gold_file = "./temp_files/gold_temp.txt"
    predict_segment = "./temp_files/seg_temp.txt"
    # gold_text0 = "./temp_files/ref0"
    # gold_text1 = "./temp_files/ref1"
    gold_text = "./temp_files/gold_text.txt"
    origin_file = "./temp_files/origin_text.txt"
    dict = {}
    dict["table"] = {"name":[], "precision":[], "recall":[], "f0.5":[]}
    # correct_file = "../NLPCC_file/mini_gold.01"
    # with open(predict_file, "r", encoding="utf-8") as prediction, open(correct_file, "r", encoding="utf-8") as correction, open(gold_file, "w", encoding="utf-8") as fo:
    with open(correct_file, "r", encoding="utf-8") as correction:
        first_line = correction.readline()
    if first_line[0] == "S" and first_line[1] == " ":
        m22m2_w_correct(correct_file, gold_file, gold_text, origin_file)
        # m22m2_multireference(correct_file, gold_file, gold_text0, gold_text1, origin_file)
    else:
        with open(correct_file, "r", encoding="utf-8") as seq, open(gold_file, "w", encoding="utf-8") as out_m2, \
                open(gold_text, "w", encoding="utf-8") as gold_text, open(origin_file, "w", encoding="utf-8") as origin_file:
            for line in seq:
                line = line.strip()
                # print(line)
                content = line.split('\t')
                origin_text = content[0]
                origin_file.write(origin_text + "\n")
                correct_text = content[1]
                gold_text.write("".join(correct_text.split()))
                gold_text.write("\n")
                # origin_text, correct_text, _ = line.split('\t')
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                out_m2.write("S " + " ".join(orig_cut) + "\n")
                m2edit = seq2m2(origin_text, correct_text)
                out_m2.write(m2edit)
                out_m2.write("\n")
    segment(predict_file, predict_segment)
    source_sentences, gold_edits = load_annotation(gold_file)
    system_segment = []
    with open(predict_segment, "r", encoding="utf-8") as f:
        for line in f.readlines():
            sent_seg = line.strip()
            system_segment.append(sent_seg)
    predict_edits, select_gold_edits, p, r, f1 = levenshtein.batch_multi_pre_rec_f1(system_segment, source_sentences,
                                                                                    gold_edits)
    dict["table"]["name"].append("总体")
    dict["table"]["precision"].append(round(p*100, 2))
    dict["table"]["recall"].append(round(r * 100, 2))
    dict["table"]["f0.5"].append(round(f1 * 100, 2))
    # dict["table"]["总体"] = {"precision": round(p*100, 2), "recall": round(r*100, 2), "f0.5": round(f1*100, 2)}
    map = word_class_map2
    labels = ["其他"]
    for key in map:
        labels.append(key)
    for label in labels:
        filter_predict_edits, filter_gold_edits = filter_with_label_map(predict_edits, select_gold_edits, label, map)
        p, r, f1 = levenshtein.batch_label_multi_pre_rec_f1(filter_predict_edits, filter_gold_edits)
        dict["table"]["name"].append(label)
        dict["table"]["precision"].append(round(p * 100, 2))
        dict["table"]["recall"].append(round(r * 100, 2))
        dict["table"]["f0.5"].append(round(f1 * 100, 2))
        dict["table"][label] = {"precision": round(p * 100, 2), "recall": round(r * 100, 2), "f0.5": round(f1 * 100, 2)}

    os.remove(gold_file)
    os.remove(predict_segment)

    # dict["BLEU"] = round(calculate_bleu_score(predict_file, gold_text), 2)
    gleu_gold = "./temp_files/gleu_gold.txt"
    gleu_origin = "./temp_files/gleu_origin.txt"
    gleu_predict = "./temp_files/gleu_predict.txt"
    gleu_process(gold_text, gleu_gold)
    gleu_process(origin_file, gleu_origin)
    gleu_process(predict_file, gleu_predict)
    dict["GLEU"] = round(float(compute_gleu_score(gleu_gold, gleu_origin, gleu_predict, 4)), 3)
    print("GLEU:")
    print(dict["GLEU"])

    # subprocess.call(["D:\\Anaconda\\python.exe","evaluate_pt_m2.py", "--source", "data/source", "--reference", "data/reference", \
    #                  "--hypothesis", "data/hypothesis", "--output", "data/output", "--base", "sentm2", "--scorer", "bertscore", \
    #                  "--model_type", "bert-base-uncased"], cwd="E:\\嘎嘎要学习了！\\新中文\\demo\\m2scorer\\scripts\\PT_M2")
    # with open("PT_M2/data/output", "r") as f:
    #     dict["PTM2"] = round(float(f.readlines()[0].strip().split("score=")[1]), 3)

    print(dict)

    return dict

def dataset_evaluate(correct_file):
    gold_file = "./dataset_temp_files/gold_temp.txt"
    gold_text = "./dataset_temp_files/gold_text.txt"
    origin_file = "./dataset_temp_files/origin_text.txt"

    with open(correct_file, "r", encoding="utf-8") as correction:
        first_line = correction.readline()
    if first_line[0] == "S" and first_line[1] == " ":
        m22m2_w_correct(correct_file, gold_file, gold_text, origin_file)
        # m22m2_multireference(correct_file, gold_file, gold_text0, gold_text1, origin_file)
    else:
        with open(correct_file, "r", encoding="utf-8") as seq, open(gold_file, "w", encoding="utf-8") as out_m2, \
                open(gold_text, "w", encoding="utf-8") as gold_text, open(origin_file, "w", encoding="utf-8") as origin_file:
            for line in seq:
                line = line.strip()
                # print(line)
                content = line.split('\t')
                origin_text = content[0]
                origin_file.write(origin_text + "\n")
                correct_text = content[1]
                gold_text.write(correct_text)
                gold_text.write("\n")
                # origin_text, correct_text, _ = line.split('\t')
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                out_m2.write("S " + " ".join(orig_cut) + "\n")
                m2edit = seq2m2(origin_text, correct_text)
                out_m2.write(m2edit)
                out_m2.write("\n")
    source_sentences, gold_edits = load_annotation(gold_file)
    label_count = {'其他': 0}
    total_edit_count = 0
    for key in word_class_map2:
        label_count[key] = 0
    for sentence in gold_edits:
        for annotator in sentence:
            for edit in sentence[annotator]:
                label_count[edit[4]] += 1
                total_edit_count += 1
    label_count.pop('其他', None)
    data = {"name":[], "千分比":[]}
    for key in label_count:
        data["name"].append(key)
        data["千分比"].append(round(label_count[key]*1000/total_edit_count, 2))

    print(data)

    return data


if __name__ == '__main__':
    orig = "不管是从口、眼、鼻子进去，会伤害身体的建康。"
    cor = "不管是从口、眼还是鼻子进去，都会伤害身体的健康。"
    print(predict(orig, cor))
    predict_file = "../NLPCC_file/without_segment/bartBaseline.txt"
    # predict_file = "../LLM/NLPCC/without_seg/qwen_0shot.txt"
    correct_file = "../NLPCC_file/gold.01"
    # correct_file = "../NLPCC_file/CGECEA_gram.01"
    print (evaluate(predict_file, correct_file))
    # gleu_gold = "./temp_files/gleu_gold.txt"
    # gleu_origin = "./temp_files/gleu_origin.txt"
    # gleu_predict = "./temp_files/gleu_predict.txt"
    # gleu_process(predict_file, gleu_predict)
    # gleu_score = round(float(compute_gleu_score(gleu_gold, gleu_origin, gleu_predict, 4)), 5)
    # print("GLEU:")
    # print(gleu_score)
    # correct_file = "../NLPCC_file/gold.01"
    # print (dataset_evaluate(correct_file))
    # gold_text = "./temp_files/gold_text.txt"
    # origin_file = "./temp_files/origin_text.txt"
    # predict_file = "../NLPCC_file/without_segment/lasertagger2.txt"
    # gleu_gold = "./temp_files/gleu_gold.txt"
    # gleu_origin = "./temp_files/gleu_origin.txt"
    # gleu_predict = "./temp_files/gleu_predict.txt"
    # gleu_process(gold_text, gleu_gold)
    # gleu_process(origin_file, gleu_origin)
    # gleu_process(predict_file, gleu_predict)
    # print(compute_gleu_score(gleu_gold, gleu_origin, gleu_predict, 4))
