import jieba
import jieba.posseg as pseg

def segment(file_in, file_out):
    segment_text = ""
    with open(file_in, "r", encoding="utf-8") as fi:
        for line in fi.readlines():
            sent = line.strip()
            # sent = line.strip().split()[1]
            # if sent == "prediction":
            #     continue
            # sent_seg = jieba.lcut(sent)
            sent_seg = pseg.cut(sent)
            sent_words = []
            for word, _ in sent_seg:
                sent_words.append(word)
            segment_text += " ".join(sent_words) + "\n"
    with open(file_out, "w", encoding="utf-8") as fo:
        fo.write(segment_text)


if __name__ == "__main__":
    files_in = ["../LLM/NLPCC/without_seg/char_5_vote.txt"]
    for file in files_in:
        # file_out = "../NLPCC_file/Jieba/"+file.split("/")[-1].split(".")[0]+"_segment2.txt"
        file_out = "../LLM/NLPCC/jieba_seg/char_5_vote.txt"
        segment(file, file_out)