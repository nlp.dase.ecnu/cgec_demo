import sys
import levenshtein
from getopt import getopt
from util import paragraphs
from util import smart_open
import jieba
from gramMatch import words_matching, word_patterns, word_class_map1, word_class_map2
import pandas as pd

def load_annotation(gold_file):
    source_sentences = []
    gold_edits = []
    fgold = open(gold_file,encoding="utf-8")
    puffer = fgold.read()
    fgold.close()
    # puffer = puffer.decode('utf8')
    for item in paragraphs(puffer.splitlines(True)):
        item = item.splitlines(False)
        sentence = [line[2:].strip() for line in item if line.startswith('S ')]
        assert sentence != []
        annotations = {}
        for line in item[1:]:
            if line.startswith('I ') or line.startswith('S '):
                continue
            assert line.startswith('A ')
            line = line[2:]
            fields = line.split('|||')
            start_offset = int(fields[0].split()[0])
            end_offset = int(fields[0].split()[1])
            etype = fields[1]
            if etype == 'noop':
                start_offset = -1
                end_offset = -1
            corrections =  [c.strip() if c != '-NONE-' else '' for c in fields[2].split('||')]
            grammar = fields[3]
            # NOTE: start and end are *token* offsets
            original = ' '.join(' '.join(sentence).split()[start_offset:end_offset])
            annotator = int(fields[5])
            if annotator not in list(annotations.keys()):
                annotations[annotator] = []
            annotations[annotator].append((start_offset, end_offset, original, corrections, grammar))
        tok_offset = 0
        for this_sentence in sentence:
            tok_offset += len(this_sentence.split())
            source_sentences.append(this_sentence)
            this_edits = {}
            for annotator, annotation in annotations.items():
                this_edits[annotator] = [edit for edit in annotation if edit[0] <= tok_offset and edit[1] <= tok_offset and edit[0] >= 0 and edit[1] >= 0]
            if len(this_edits) == 0:
                this_edits[0] = []
            gold_edits.append(this_edits)
    return (source_sentences, gold_edits)

def filter_with_label(predict_edits, select_gold_edits, label_id):
    predict_edits_w_label = []
    gold_edits_w_label = []
    for sentence_edit in predict_edits:
        predict_sentence_edit_w_label = []
        for edit in sentence_edit:
            target_words = edit[3]
            # print(target_words)
            labels, matches = words_matching(target_words)
            if label_id in labels:
                predict_sentence_edit_w_label.append(edit)
        # print(sentence_edit_w_label)
        # print(labels)
        # print(matches)
        predict_edits_w_label.append(predict_sentence_edit_w_label)
    # print(predict_edits_w_label)
    for sentence_gold in select_gold_edits:
        gold_sentence_edit_w_label = []
        for gold_edit in sentence_gold:
            target_words = " ".join(gold_edit[3])
            labels, matches = words_matching(target_words)
            if label_id in labels:
                gold_sentence_edit_w_label.append(gold_edit)
        gold_edits_w_label.append(gold_sentence_edit_w_label)
    # print(gold_edits_w_label)
    return predict_edits_w_label, gold_edits_w_label

def filter_with_label_map(predict_edits, select_gold_edits, label, map):
    predict_edits_w_label = []
    gold_edits_w_label = []
    for sentence_edit in predict_edits:
        predict_sentence_edit_w_label = []
        for edit in sentence_edit:
            target_words = edit[3]
            if target_words == "":
                target_words = edit[2]
            # print(target_words)
            predict_label = words_matching(target_words, map)
            # print(labels)
            # for i in map[label]:
            #     # label_id = str(i)
            #     if i in labels:
            #         predict_sentence_edit_w_label.append(edit)
            #         break
            if predict_label == label:
                predict_sentence_edit_w_label.append(edit)
        # print(sentence_edit_w_label)
        # print(labels)
        # print(matches)
        predict_edits_w_label.append(predict_sentence_edit_w_label)
    # print(predict_edits_w_label)
    for sentence_gold in select_gold_edits:
        gold_sentence_edit_w_label = []
        for gold_edit in sentence_gold:
            # target_words = " ".join(gold_edit[3])
            # labels, matches = words_matching(target_words)
            # for i in map[label]:
            #     # label_id = str(i)
            #     if i in labels:
            #         gold_sentence_edit_w_label.append(gold_edit)
            #         break
            if gold_edit[4] == label:
                gold_sentence_edit_w_label.append(gold_edit)
        gold_edits_w_label.append(gold_sentence_edit_w_label)
    # print(gold_edits_w_label)
    return predict_edits_w_label, gold_edits_w_label

def filter_with_len(system_segment, source_sentences, gold_edits, start, end):
    filter_system_segment = []
    filter_source_sentences = []
    filter_gold_edits = []
    for system_seg, source_sentence, gold_edit in zip(system_segment, source_sentences, gold_edits):
        src = "".join(source_sentence.split())
        if len(src)>=start and len(src)<end:
            filter_system_segment.append(system_seg)
            filter_source_sentences.append(source_sentence)
            filter_gold_edits.append(gold_edit)
    return filter_system_segment, filter_source_sentences, filter_gold_edits

if __name__ == "__main__":
    #gold edit
    gold_file = "../NLPCC_file/gold.01"
    #auto edit
    # gold_file = "../LLM/NLPCC/CGECEA_gram.01"
    predict_segment_files = ["../NLPCC_file/PKUNLP/bartSyngec_segment.txt",
                             "../NLPCC_file/PKUNLP/bartBaseline_segment.txt",
                             "../NLPCC_file/PKUNLP/gector_HSKsegment.txt",
                             "../NLPCC_file/PKUNLP/lasertagger2_segment.txt",
                             "../NLPCC_file/PKUNLP/chatGPT_segment.txt",
                             "../LLM/NLPCC/pkunlp_seg/chatGLM0shot_segment.txt",
                             "../LLM/NLPCC/pkunlp_seg/qwen0shot_segment.txt",
                             "../LLM/NLPCC/pkunlp_seg/char_5_vote.txt"]

    result_file = "../results/sent_lengt_eval_18.csv"
    # predict_segment_files = ["../NLPCC_file/Jieba/bartBaseline_segment2.txt","../NLPCC_file/Jieba/bartSyngec_segment2.txt","../NLPCC_file/Jieba/transformerBaseline_segment2.txt","../NLPCC_file/Jieba/transformerSyngec_segment2.txt", "../NLPCC_file/Jieba/gector_segment2.txt", "../NLPCC_file/Jieba/lasertagger2_segment2.txt"]
    source_sentences, gold_edits = load_annotation(gold_file)
    # print(gold_edits)

    #predict sentences
    # system_sentences = []
    # system_sentences.append("冬阴功是泰国最著名的菜之一，虽然它不是很豪华，但它的味确实让人上瘾，做法也不难、不复杂。")
    # system_sentences.append("这样，你就会尝到泰国人爱死的味道。")
    # system_sentences.append("这部电影不仅在国内，在国外也很有名。")

    # seg = pkuseg.pkuseg()
    dict = {}
    for predict_segment_file in predict_segment_files:
        model_name = predict_segment_file.split('/')[-1].split('_')[0]
        dict[model_name] = {}
        system_segment = []
        # for sent in system_sentences:
        #     cut_sentence = jieba.lcut(sent)
        #     sent_seg = ' '.join(cut_sentence)
        #     system_segment.append(sent_seg)
        with open(predict_segment_file, "r", encoding="utf-8") as f:
            for line in f.readlines():
                sent_seg = line.strip()
                system_segment.append(sent_seg)
        # print(system_segment)

        # p, r, f1 = levenshtein.batch_multi_pre_rec_f1(system_segment, source_sentences, gold_edits)

        # label_id = 0


        predict_edits, select_gold_edits, p, r, f1 = levenshtein.batch_multi_pre_rec_f1(system_segment, source_sentences, gold_edits)
        # print(predict_edits)
        # print(select_gold_edits)

        #计算detection
        # predict_cnt = 0
        # gold_cnt = 0
        # correct_cnt = 0
        # for m_pre_edits, m_gold_edits in zip(predict_edits, select_gold_edits):
        #     pre_spans = []
        #     gold_spans = []
        #     for edit in m_pre_edits:
        #         pre_spans.append(edit[:2])
        #     for edit in m_gold_edits:
        #         gold_spans.append(edit[:2])
        #     predict_cnt += len(pre_spans)
        #     gold_cnt += len(gold_spans)
        #     for span in pre_spans:
        #         if span in gold_spans:
        #             correct_cnt+=1
        #
        # d_precision = correct_cnt / predict_cnt
        # d_recall = correct_cnt / gold_cnt
        # d_f = (1 + 0.5 * 0.5) * d_precision * d_recall / (d_recall + 0.5 * 0.5 * d_precision)
        # print("d_Precision   : %.4f" % d_precision)
        # print("d_Recall      : %.4f" % d_recall)
        # print("d_F0.5       : %.4f" % (d_f))

        # print("Precision   : %.4f" % p)
        # print("Recall      : %.4f" % r)
        # print("F1       : %.4f" % (f1))
        # if "total_p" not in dict[model_name]:
        #     dict[model_name]["total_p"] = []
        #     dict[model_name]["total_r"] = []
        #     dict[model_name]["total_f0.5"] = []
        dict[model_name]["total_p"] = p
        dict[model_name]["total_r"] = r
        dict[model_name]["total_f0.5"] = f1
        print('-----total-------')
        print(p)
        print(r)
        print(f1)
        print('-----total-------')

        short_system_segment, short_source_sentences, short_gold_edits = filter_with_len(system_segment,
                                                                                         source_sentences, gold_edits,
                                                                                         0, 18)
        _, _, p_short, r_short, f1_short = levenshtein.batch_multi_pre_rec_f1(short_system_segment, short_source_sentences,
                                                                        short_gold_edits)
        dict[model_name]['short_p'] = p_short
        dict[model_name]["short_r"] = r_short
        dict[model_name]["short_f0.5"] = f1_short
        # print('-----short-------')
        # print(p_short)
        # print(r_short)
        # print(f1_short)
        # print('-----short-------')

        medium_system_segment, medium_source_sentences, medium_gold_edits = filter_with_len(system_segment,
                                                                                         source_sentences, gold_edits,
                                                                                         18, 40)
        _, _, p_medium, r_medium, f1_medium = levenshtein.batch_multi_pre_rec_f1(medium_system_segment, medium_source_sentences,
                                                                        medium_gold_edits)
        dict[model_name]['medium_p'] = p_medium
        dict[model_name]["medium_r"] = r_medium
        dict[model_name]["medium_f0.5"] = f1_medium
        # print('-----medium-------')
        # print(p_medium)
        # print(r_medium)
        # print(f1_medium)
        # print('-----medium-------')
        # long_system_segment, long_source_sentences, long_gold_edits = filter_with_len(system_segment,
        #                                                                                     source_sentences,
        #                                                                                     gold_edits,
        #                                                                                     40, 300)
        # _, _, p_long, r_long, f1_long = levenshtein.batch_multi_pre_rec_f1(long_system_segment, long_source_sentences,
        #                                                              long_gold_edits)
        # dict[model_name]['long_p'] = p_long
        # dict[model_name]["long_r"] = r_long
        # dict[model_name]["long_f0.5"] = f1_long
        # print('-----long-------')
        # print(p_long)
        # print(r_long)
        # print(f1_long)
        # print('-----long-------')
        # superlong_system_segment, superlong_source_sentences, superlong_gold_edits = filter_with_len(system_segment,
        #                                                                               source_sentences,
        #                                                                               gold_edits,
        #                                                                               36, 300)
        # _, _, p_superlong, r_superlong, f1_superlong = levenshtein.batch_multi_pre_rec_f1(superlong_system_segment, superlong_source_sentences,
        #                                                                    superlong_gold_edits)
        # dict[model_name]['superlong_p'] = p_superlong
        # dict[model_name]["superlong_r"] = r_superlong
        # dict[model_name]["superlong_f0.5"] = f1_superlong
        # print('-----superlong-------')
        # print(p_superlong)
        # print(r_superlong)
        # print(f1_superlong)
        # print('-----superlong-------')

        # label_id = 0 # 错别字
        # filter_predict_edits, filter_gold_edits = filter_with_label(predict_edits, select_gold_edits, label_id)
        # p_label, r_label, f1_label = levenshtein.batch_label_multi_pre_rec_f1(filter_predict_edits, filter_gold_edits)
        # dict[model_name]["0_p"] = p_label
        # dict[model_name]["0_r"] = r_label
        # dict[model_name]["0_f0.5"] = f1_label

        #细粒度评估
        # map = word_class_map2
        # labels = ["其他"]
        # for key in map:
        #     labels.append(key)
        # for label in labels:
        #     # label_id = int(i)
        #     filter_predict_edits, filter_gold_edits = filter_with_label_map(predict_edits, select_gold_edits, label, map)
        #     p_label, r_label, f1_label = levenshtein.batch_label_multi_pre_rec_f1(filter_predict_edits, filter_gold_edits)
        #     # print("Precision   : %.4f" % p_label)
        #     # print("Recall      : %.4f" % r_label)
        #     # print("F0.5       : %.4f" % (f1_label))
        #     dict[model_name][label+"_p"] = p_label
        #     dict[model_name][label+"_r"] = r_label
        #     dict[model_name][label+"_f0.5"] = f1_label
            # if int(i) >2:
            #     break

    #结果存储
    df = pd.DataFrame(dict)
    print(df)
    df.to_csv(result_file, encoding="utf_8_sig")


