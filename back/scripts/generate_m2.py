from alignment import *

def seq2m2(origin_text, correct_text, id=0):
    orig = pseg.cut(origin_text)
    cor = pseg.cut(correct_text)
    orig_cut = []
    orig_pos = []
    orig_gram = []
    cor_cut = []
    cor_pos = []
    cor_gram = []
    map = word_class_map2
    for word, flag in orig:
        orig_cut.append(word)
        orig_pos.append(flag)
        orig_gram.append(word_matching(word))
    for word, flag in cor:
        cor_cut.append(word)
        cor_pos.append(flag)
        cor_gram.append(word_matching(word))
    # print(" ".join(orig_cut))
    # print(cor_cut)
    lev = False
    cost_matrix, op_matrix = align(orig_cut, cor_cut, orig_pos, cor_pos, orig_gram, cor_gram, lev)
    align_seq = get_cheapest_align_seq(op_matrix)
    alist = []
    for s in align_seq:
        alist.append(s[0])
    # print(alist)
    # print("----------------------")
    for s in align_seq:
        if s[0]!="M":
            print(s)
    # print(align_seq)
    edits = get_rule_edits(align_seq, orig_cut, cor_cut, orig_pos, cor_pos)
    m2edits = ""
    for edit in edits:
        # print(edit)
        m2edits += edit.to_m2(id) + "\n"
    return m2edits

def m22m2(file_in, file_out):
    with open(file_in, "r", encoding="utf-8") as m2, open(file_out, "w", encoding="utf-8") as out_m2:
        # Store the current m2_block here
        m2_block = []
        # Loop through m2 lines
        for line in m2:
            line = line.strip()
            # If the line isn't empty, add it to the m2_block
            if line:
                m2_block.append(line)
            # Otherwise, process the complete blocks
            else:
                # Write the original text to the output M2 file
                origin = m2_block[0][2:].split()
                origin_text = "".join(origin)
                # Parse orig with spacy
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                out_m2.write("S " + " ".join(orig_cut) + "\n")
                # Simplify the edits and sort by coder id
                edit_dict = simplify_edits(m2_block[1:])
                # Loop through coder ids
                for id, raw_edits in sorted(edit_dict.items()):
                    # If the first edit is a noop
                    if raw_edits[0][2] == "noop":
                        # Write the noop and continue
                        out_m2.write(noop_edit(id) + "\n")
                        continue
                    # Apply the edits to generate the corrected text
                    # Also redefine the edits as orig and cor token offsets
                    correct_text, gold_edits = get_cor_and_edits(origin, raw_edits)
                    m2edit = seq2m2(origin_text, correct_text, id)
                    out_m2.write(m2edit)
                # Write a newline when there are no more edits
                out_m2.write("\n")
                m2_block = []

def m22m2_multireference(m2_file, out_m2_file, out_gold_file0, out_gold_file1, origin_file):
    with open(m2_file, "r", encoding="utf-8") as m2, open(out_m2_file, "w", encoding="utf-8") as out_m2, \
            open(out_gold_file0, "w", encoding="utf-8") as gold_text0, open(out_gold_file1, "w", encoding="utf-8") as gold_text1,\
            open(origin_file, "w", encoding="utf-8") as origin_file:
        # Store the current m2_block here
        m2_block = []
        # Loop through m2 lines
        for line in m2:
            line = line.strip()
            # If the line isn't empty, add it to the m2_block
            if line:
                m2_block.append(line)
            # Otherwise, process the complete blocks
            else:
                # Write the original text to the output M2 file
                origin = m2_block[0][2:].split()
                origin_text = "".join(origin)
                origin_file.write(" ".join(origin) + "\n")
                # print(origin_text)
                out_text0 = " ".join(origin)
                out_text1 = " ".join(origin)
                # Parse orig with spacy
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                out_m2.write("S " + " ".join(orig_cut) + "\n")
                # Simplify the edits and sort by coder id
                edit_dict = simplify_edits(m2_block[1:])
                # Loop through coder ids
                for id, raw_edits in sorted(edit_dict.items()):
                    print(id)
                    # If the first edit is a noop
                    if raw_edits[0][2] == "noop":
                        # Write the noop and continue
                        out_m2.write(noop_edit(id) + "\n")
                        continue
                    # Apply the edits to generate the corrected text
                    # Also redefine the edits as orig and cor token offsets
                    correct_text, gold_edits = get_cor_and_edits(origin, raw_edits)
                    # print(correct_text)
                    if (correct_text):
                        if id == "0":
                            out_text0 = correct_text
                            # print(out_text0)
                        elif id == "1":
                            out_text1 = correct_text
                            print(out_text1)
                            print(raw_edits)
                    m2edit = seq2m2(origin_text, correct_text, id)
                    out_m2.write(m2edit)
                # Write a newline when there are no more edits
                out_m2.write("\n")
                gold_text0.write(out_text0)
                gold_text1.write(out_text1)
                gold_text0.write("\n")
                gold_text1.write("\n")
                # Reset the m2 block
                m2_block = []

def m22m2_w_correct(m2_file, out_m2_file, out_gold_file, origin_file):
    with open(m2_file, "r", encoding="utf-8") as m2, open(out_m2_file, "w", encoding="utf-8") as out_m2, \
            open(out_gold_file, "w", encoding="utf-8") as gold_text, open(origin_file, "w", encoding="utf-8") as origin_file:
        # Store the current m2_block here
        m2_block = []
        # Loop through m2 lines
        for line in m2:
            line = line.strip()
            # If the line isn't empty, add it to the m2_block
            if line:
                m2_block.append(line)
            # Otherwise, process the complete blocks
            else:
                # Write the original text to the output M2 file
                origin = m2_block[0][2:].split()
                origin_text = "".join(origin)
                origin_file.write(origin_text + "\n")
                # print(origin_text)
                out_text = origin_text
                # Parse orig with spacy
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                out_m2.write("S " + " ".join(orig_cut) + "\n")
                # Simplify the edits and sort by coder id
                edit_dict = simplify_edits(m2_block[1:])
                # Loop through coder ids
                for id, raw_edits in sorted(edit_dict.items()):
                    # If the first edit is a noop
                    if raw_edits[0][2] == "noop":
                        # Write the noop and continue
                        out_m2.write(noop_edit(id) + "\n")
                        continue
                    # Apply the edits to generate the corrected text
                    # Also redefine the edits as orig and cor token offsets
                    correct_text, gold_edits = get_cor_and_edits(origin, raw_edits)
                    # print(correct_text)
                    if (correct_text): out_text = correct_text
                    m2edit = seq2m2(origin_text, correct_text, id)
                    out_m2.write(m2edit)
                # Write a newline when there are no more edits
                out_m2.write("\n")
                gold_text.write("".join(out_text.split()))
                gold_text.write("\n")
                # Reset the m2 block
                m2_block = []

def simplify_edits(edits):
    edit_dict = {}
    for edit in edits:
        edit = edit.split("|||")
        span = edit[0][2:].split() # [2:] ignore the leading "A "
        start = int(span[0])
        end = int(span[1])
        cat = edit[1]
        cor = edit[2]
        id = edit[-1]
        # Save the useful info as a list
        proc_edit = [start, end, cat, cor]
        # Save the proc_edit inside the edit_dict using coder id
        if id in edit_dict.keys():
            edit_dict[id].append(proc_edit)
        else:
            edit_dict[id] = [proc_edit]
    return edit_dict

def noop_edit(id=0):
    return "A -1 -1|||noop|||-NONE-|||REQUIRED|||-NONE-|||"+str(id)

def get_cor_and_edits(orig, edits):
    # Copy orig; we will apply edits to it to make cor
    cor = orig.copy()
    # print(cor)
    new_edits = []
    offset = 0
    # Sort the edits by offsets before processing them
    edits = sorted(edits, key=lambda e:(e[0], e[1]))
    # Loop through edits: [o_start, o_end, cat, cor_str]
    for edit in edits:
        # print(edit)
        o_start = edit[0]
        o_end = edit[1]
        cat = edit[2]
        cor_toks = edit[3].split()
        # Detection edits
        if cat in {"Um", "UNK"}:
            # Save the pseudo correction
            det_toks = cor_toks[:]
            # But temporarily overwrite it to be the same as orig
            cor_toks = orig.split()[o_start:o_end]
        if cat in {"R"} and cor_toks == ["-NONE-"]:
            cor_toks = ""
        # Apply the edits
        cor[o_start+offset:o_end+offset] = cor_toks
        # Get the cor token start and end offsets in cor
        c_start = o_start+offset
        c_end = c_start+len(cor_toks)
        # Keep track of how this affects orig edit offsets
        offset = offset-(o_end-o_start)+len(cor_toks)
        # Detection edits: Restore the pseudo correction
        if cat in {"Um", "UNK"}: cor_toks = det_toks
        # Update the edit with cor span and save
        new_edit = [o_start, o_end, c_start, c_end, cat, " ".join(cor_toks)]
        new_edits.append(new_edit)
    return " ".join(cor), new_edits

def m22seq(m2_file, seq_file):
    with open(m2_file, "r", encoding="utf-8") as m2, open(seq_file, "w", encoding="utf-8") as out_seq:
        # Store the current m2_block here
        m2_block = []
        # Loop through m2 lines
        for line in m2:
            line = line.strip()
            # If the line isn't empty, add it to the m2_block
            if line:
                m2_block.append(line)
            # Otherwise, process the complete blocks
            else:
                # Write the original text to the output M2 file
                origin = m2_block[0][2:].split()
                origin_text = "".join(origin)
                # Parse orig with spacy
                orig = pseg.cut(origin_text)
                orig_cut = []
                orig_pos = []
                for word, flag in orig:
                    orig_cut.append(word)
                    orig_pos.append(flag)
                # Simplify the edits and sort by coder id
                edit_dict = simplify_edits(m2_block[1:])
                # Loop through coder ids
                correct_text = origin_text
                for id, raw_edits in sorted(edit_dict.items()):
                    # If the first edit is a noop
                    # if raw_edits[0][2] == "noop":
                    #     # Write the noop and continue
                    #     out_m2.write(noop_edit(id) + "\n")
                    #     continue
                    # Apply the edits to generate the corrected text
                    # Also redefine the edits as orig and cor token offsets
                    correct_text, gold_edits = get_cor_and_edits(origin, raw_edits)
                    # m2edit = seq2m2(origin_text, correct_text, id)
                    correct_text = "".join(correct_text.split())
                out_seq.write(correct_text)
                # Write a newline when there are no more edits
                out_seq.write("\n")
                m2_block = []

# def import_edit(orig, cor, edit, min=True, old_cat=False):
#     # Undefined error type
#     if len(edit) == 4:
#         edit = Edit(orig, cor, edit)
#     # Existing error type
#     elif len(edit) == 5:
#         edit = Edit(orig, cor, edit[:4], edit[4])
#     # Unknown edit format
#     else:
#         raise Exception("Edit not of the form: "
#             "[o_start, o_end, c_start, c_end, (cat)]")
#     # Minimise edit
#     if min:
#         edit = edit.minimise()
#         # Classify edit
#         if not old_cat:
#             edit = classify(edit)
#         return edit

if __name__ == "__main__":
    file_src = "../NLPCC_file/src.txt"
    file_predict = "../NLPCC_file/without_segment/lasertagger2.txt"
    file_out = "../LLM/NLPCC/predict2m2/lasertagger.CGECEA"
    with open(file_src, "r", encoding="utf-8-sig") as src, open(file_predict, "r", encoding="utf-8") as cor,open(file_out, "w", encoding="utf-8") as out_m2:
        for origin_text, correct_text in zip(src, cor):
            # origin_text, correct_text, _ = line.split('\t')
            origin_text = origin_text.strip()
            correct_text = correct_text.strip()
            orig = pseg.cut(origin_text)
            orig_cut = []
            orig_pos = []
            for word, flag in orig:
                orig_cut.append(word)
                orig_pos.append(flag)
            out_m2.write("S " + " ".join(orig_cut) + "\n")
            m2edit = seq2m2(origin_text, correct_text)
            out_m2.write(m2edit)
            out_m2.write("\n")


    #
    # file_m2 = "../LLM/NLPCC/predict2m2/char_5_vote.CGECEA"
    # file_out = "../LLM/NLPCC/without_seg/char_5_vote.txt"
    # m22seq(file_m2, file_out)