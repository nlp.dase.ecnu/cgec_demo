import json

def switch(sent, ord):
    temp = ""
    for i in ord:
        temp += sent[i]
    return temp

def edit(sent_list, o_start, o_end, cor_toks, offset):
    sent_list[o_start + offset:o_end + offset] = cor_toks
    offset = offset - (o_end - o_start) + len(cor_toks)
    return sent_list, offset

if __name__ == "__main__":
    # source = []
    # target = []
    with open("FCGEC_valid.json", "r", encoding="utf-8") as fi, open("FCGEC_valid.txt", "w", encoding="utf-8") as fo:
        data = json.load(fi)
        for key in data:
            src_sent = data[key]["sentence"]
            annotators = json.loads(data[key]["operation"])
            cor_sent = src_sent
            for annotator in annotators:
                if "Switch" in annotator: cor_sent = switch(src_sent, annotator["Switch"])
                cor_list = [char for char in cor_sent]
                offset = 0
                for op in annotator:
                    if op == "Delete":
                        for unit in annotator[op]:
                            o_start = unit
                            o_end = unit+1
                            cor_toks = []
                            cor_list, offset = edit(cor_list, o_start, o_end, cor_toks, offset)
                    elif op == "Insert":
                        for unit in annotator[op]:
                            o_start = unit["pos"]+1
                            o_end = o_start
                            cor_text = unit["label"] if type(unit["label"])==str else unit["label"][0]
                            cor_toks = [char for char in cor_text]
                            cor_list, offset = edit(cor_list, o_start, o_end, cor_toks, offset)
                    elif op == "Modify":
                        for unit in annotator[op]:
                            o_start = unit["pos"]
                            o_end = o_start + int(unit["tag"].split("+")[0].split("_")[1])
                            cor_text = unit["label"] if type(unit["label"])==str else unit["label"][0]
                            cor_toks = [char for char in cor_text]
                            cor_list, offset = edit(cor_list, o_start, o_end, cor_toks, offset)
                cor_sent = "".join(cor_list)
                # print(src_sent)
                # print(annotator)
                # print("".join(cor_list))
                fo.write(src_sent)
                fo.write("\t")
                fo.write(cor_sent)
                fo.write("\n")